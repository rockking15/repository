﻿using System;
using System.Collections.Generic;
using System.Linq;
using UkadTask.BusinessObjects;
using UkadTask.Context;
using UkadTask.IRepository;

namespace UkadTask.Repository
{
    public class Repo : IRepo
    {
        private readonly UkadTaskContext _context = new UkadTaskContext();
        private bool _disposed;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public IEnumerable<EnteredLink> GetLinksList()
        {
            return _context.Links.ToList();
        }

        public void CreateLink(EnteredLink enteredLink)
        {
            _context.Links.Add(enteredLink);
        }

        public void CreateResponseTime(List<ResponseTime> responseTimes)
        {
            _context.ResponseTimes.AddRange(responseTimes);
        }

        public IEnumerable<ResponseTime> GetResponseTimeListFromDb(string testT)
        {
            return
                _context.ResponseTimes.AsEnumerable()
                    .Where(f => f.TestTime.ToString("yyyy-MM-dd HH:mm:ss") == testT)
                    .ToList();
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }
    }
}