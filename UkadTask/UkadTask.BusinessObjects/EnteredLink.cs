﻿using System;

namespace UkadTask.BusinessObjects
{
    public class EnteredLink
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public DateTime TestedTime { get; set; }
    }
}