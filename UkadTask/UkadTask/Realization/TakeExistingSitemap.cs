﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Xml;
using UkadTask.BusinessObjects;
using UkadTask.ViewModels;

namespace UkadTask.Realization
{
    public class TakeExistingSitemap
    {
        public List<ResponseTime> TakeSitemapOrCreateIfNotExist(string url, XmlDocument xmlDoc,
            EnteredLinkViewModel model)
        {
            var sitemap = new CreateSitemapIfNotExist();
            var sitemaps = new List<ResponseTime>();
            try
            {
                xmlDoc.Load(url);

                foreach (XmlNode node in xmlDoc.GetElementsByTagName("url"))
                {
                    var item = new ResponseTime
                    {
                        Url = node["loc"]?.InnerText
                    };
                    sitemaps.Add(item);
                }
            }
            catch (WebException ex) when ((ex.Response as HttpWebResponse)?.StatusCode == HttpStatusCode.NotFound)
            {
                sitemaps = sitemap.GenerateSitemap(model);
            }
            catch (XmlException)
            {
                sitemaps = sitemap.GenerateSitemap(model);
            }
            catch (Exception)
            {
                return sitemaps;
            }

            return sitemaps;
        }
    }
}