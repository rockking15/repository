﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using HtmlAgilityPack;
using UkadTask.BusinessObjects;
using UkadTask.ViewModels;

namespace UkadTask.Realization
{
    public class CreateSitemapIfNotExist
    {
        public List<ResponseTime> GenerateSitemap(EnteredLinkViewModel model)
        {
            var sitemaps = new List<ResponseTime>();
            var htmlWeb = new HtmlWeb {OverrideEncoding = Encoding.UTF8};
            var htmlDocument = htmlWeb.Load(model.Url);

            foreach (
                var link in
                    htmlDocument.DocumentNode.SelectNodes(
                        "//a[starts-with(@href, '/')] | //a[starts-with(@href, '.')] | //a[starts-with(@href, '" +
                        model.Url + "')]"))
            {
                string hrefValue;
                ResponseTime item;
                if (link.OuterHtml.Contains(model.Url))
                {
                    hrefValue = WebUtility.HtmlDecode(link.GetAttributeValue("href", string.Empty));
                    item = new ResponseTime
                    {
                        Url = hrefValue
                    };
                }
                else
                {
                    hrefValue = WebUtility.HtmlDecode(link.GetAttributeValue("href", string.Empty).Substring(1));

                    var index = hrefValue.LastIndexOf("#", StringComparison.Ordinal);
                    if (index > 0)
                        hrefValue = hrefValue.Substring(0, index);
                    item = new ResponseTime
                    {
                        Url = model.Url + hrefValue
                    };
                }

                sitemaps.Add(item);
            }

            sitemaps = sitemaps.GroupBy(l => l.Url).Select(group => group.First()).ToList();

            return sitemaps;
        }
    }
}