﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using System.Xml;
using UkadTask.BusinessObjects;
using UkadTask.IRepository;
using UkadTask.Realization;
using UkadTask.Repository;
using UkadTask.ViewModels;

namespace UkadTask.Controllers
{
    public class HomeController : Controller
    {
        private static readonly List<ResponseTime> List = new List<ResponseTime>();
        private readonly IRepo _repo;

        public HomeController()
        {
            _repo = new Repo();
        }

        [HttpGet]
        public ActionResult Index()
        {
            var model = new EnteredLinkViewModel {EnteredLinks = _repo.GetLinksList()};
            return View(model);
        }

        [HttpPost]
        public ActionResult Index(EnteredLinkViewModel model)
        {
            var date = DateTime.Now;
            if (ModelState.IsValid)
            {
                var enteredLink = new EnteredLink
                {
                    Url = model.Url,
                    TestedTime = date
                };

                _repo.CreateLink(enteredLink);
                _repo.Save();

                var url = model.Url + "/sitemap.xml";
                var xmlDoc = new XmlDocument();


                var existing = new TakeExistingSitemap();
                var sitemaps = existing.TakeSitemapOrCreateIfNotExist(url, xmlDoc, model);

                for (var i = 0; i < sitemaps.Count(); i++)
                {
                    var request = (HttpWebRequest) WebRequest.Create(sitemaps[i].Url);
                    request.ContentType = "text/xml;charset=\"utf-8\"";
                    request.Accept = "text/xml";
                    request.Method = "GET";
                    request.Proxy = null;
                    request.UserAgent = "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)";
                    request.ContentLength = 0;
                    request.UseDefaultCredentials = true;

                    var stopwatch = new Stopwatch();
                    stopwatch.Start();

                    try
                    {
                        request.GetResponse();
                    }
                    catch (WebException ex)
                    {
                        if (ex.Status == WebExceptionStatus.ProtocolError &&
                            ex.Response != null)
                        {
                            var resp = (HttpWebResponse) ex.Response;
                            if (resp.StatusCode == HttpStatusCode.NotFound)
                            {
                                ViewBag.Message = ex.Message;
                            }
                        }
                    }

                    stopwatch.Stop();
                    var item = new ResponseTime
                    {
                        CurrentResponseTime = stopwatch.ElapsedMilliseconds,
                        Url = sitemaps[i].Url,
                        TestTime = date
                    };
                    List.Add(item);
                }

                var res = (from item in List
                    where item.Url.Contains(model.Url)
                    group item by item.Url
                    into grp
                    select new
                    {
                        Name = grp.Key,
                        Max = grp.Max(rec => rec.CurrentResponseTime),
                        Min = grp.Min(rec => rec.CurrentResponseTime),
                        resp = grp.Select(rec => rec.CurrentResponseTime)
                    }).AsEnumerable().Select(x => new ResponseTime
                    {
                        Url = x.Name,
                        MaxResponseTime = x.Max,
                        MinResponseTime = x.Min,
                        CurrentResponseTime = x.resp.LastOrDefault(),
                        TestTime = date
                    }).ToList();

                var resultOrder = (from item in res
                    orderby item.CurrentResponseTime descending
                    select item).ToList();

                _repo.CreateResponseTime(resultOrder);
                _repo.Save();

                var resultModel = new ResponseTimesViewModel {ResponseTimes = resultOrder};

                return PartialView("/Views/History/_PartialResults.cshtml", resultModel);
            }
            return View(model);
        }
    }
}