﻿using System.Web.Mvc;
using UkadTask.IRepository;
using UkadTask.Repository;
using UkadTask.ViewModels;

namespace UkadTask.Controllers
{
    public class HistoryController : Controller
    {
        private readonly IRepo _repo;

        public HistoryController()
        {
            _repo = new Repo();
        }

        public PartialViewResult History(string testT)
        {
            var model = new ResponseTimesViewModel {ResponseTimes = _repo.GetResponseTimeListFromDb(testT)};
            return PartialView("_PartialResults", model);
        }
    }
}