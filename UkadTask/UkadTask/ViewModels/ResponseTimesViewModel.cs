﻿using System.Collections.Generic;
using UkadTask.BusinessObjects;

namespace UkadTask.ViewModels
{
    public class ResponseTimesViewModel
    {
        public IEnumerable<ResponseTime> ResponseTimes { get; set; }
    }
}