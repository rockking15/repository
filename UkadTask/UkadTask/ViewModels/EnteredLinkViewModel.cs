﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using UkadTask.BusinessObjects;

namespace UkadTask.ViewModels
{
    public class EnteredLinkViewModel
    {
        public IEnumerable<EnteredLink> EnteredLinks { get; set; }

        [Required]
        [DataType(DataType.Url)]
        public string Url { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime TestedTime { get; set; }
    }
}