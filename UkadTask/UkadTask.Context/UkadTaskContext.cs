﻿using System.Data.Entity;
using UkadTask.BusinessObjects;

namespace UkadTask.Context
{
    public class UkadTaskContext : DbContext
    {
        public UkadTaskContext()
            : base("name=UkadTask")
        {
        }

        public virtual DbSet<EnteredLink> Links { get; set; }
        public virtual DbSet<ResponseTime> ResponseTimes { get; set; }
    }
}