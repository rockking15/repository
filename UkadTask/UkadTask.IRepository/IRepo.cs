﻿using System;
using System.Collections.Generic;
using UkadTask.BusinessObjects;

namespace UkadTask.IRepository
{
    public interface IRepo : IDisposable
    {
        IEnumerable<EnteredLink> GetLinksList();
        void CreateLink(EnteredLink enteredLink);
        void CreateResponseTime(List<ResponseTime> responseTimes);
        IEnumerable<ResponseTime> GetResponseTimeListFromDb(string testT);
        void Save();
    }
}