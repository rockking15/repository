﻿using System.Collections.Generic;
using System.Data.Entity;
using InternetProvider.BusinessObjects;
using InternetProvider.DbContext;
using InternetProvider.RepositoryInterfaces;

namespace InternetProvider.RepositoryImplementation
{
    public class ServiceRepository : IRepository<Service>
    {
        private readonly InternetProviderContext _context;

        public ServiceRepository(InternetProviderContext dbContext)
        {
            _context = dbContext;
        }

        public IEnumerable<Service> GetAll()
        {
            return _context.Services;
        }

        public Service Get(int id)
        {
            return _context.Services.Find(id);
        }

        public void Create(Service item)
        {
            _context.Services.Add(item);
        }

        public void Update(Service item)
        {
            _context.Entry(item).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            var service = _context.Services.Find(id);
            if (service != null)
                _context.Services.Remove(service);
        }

        public List<Rate> Find(int id)
        {
            var service = _context.Services.Find(id);

            var rates = service.Rates;

            var list = new List<Rate>();
            list.AddRange(rates);

            return list;
        }
    }
}