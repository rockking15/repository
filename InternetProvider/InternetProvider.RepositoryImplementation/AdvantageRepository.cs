﻿using System.Collections.Generic;
using System.Data.Entity;
using InternetProvider.BusinessObjects;
using InternetProvider.DbContext;
using InternetProvider.RepositoryInterfaces;

namespace InternetProvider.RepositoryImplementation
{
    public class AdvantageRepository : IRepository<Advantage>
    {
        private readonly InternetProviderContext _context;

        public AdvantageRepository(InternetProviderContext dbContext)
        {
            _context = dbContext;
        }

        public IEnumerable<Advantage> GetAll()
        {
            return _context.Advantages;
        }

        public Advantage Get(int id)
        {
            return _context.Advantages.Find(id);
        }

        public void Create(Advantage item)
        {
            _context.Advantages.Add(item);
        }

        public void Update(Advantage item)
        {
            _context.Entry(item).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            var advantage = _context.Advantages.Find(id);
            if (advantage != null)
                _context.Advantages.Remove(advantage);
        }
    }
}