﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using InternetProvider.BusinessObjects;
using InternetProvider.DbContext;
using InternetProvider.RepositoryInterfaces;

namespace InternetProvider.RepositoryImplementation
{
    public class NewsRepository : IRepository<News>
    {
        private readonly InternetProviderContext _context;

        public NewsRepository(InternetProviderContext dbContext)
        {
            _context = dbContext;
        }

        public IEnumerable<News> GetAll()
        {
            return _context.Newses;
        }

        public News Get(int id)
        {
            return _context.Newses.Find(id);
        }

        public void Create(News item)
        {
            _context.Newses.Add(item);
        }

        public void Update(News item)
        {
            _context.Entry(item).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            var news = _context.Newses.Find(id);
            if (news != null)
                _context.Newses.Remove(news);
        }

        public IEnumerable<News> OrderByDescendingPublicationDate()
        {
            return _context.Newses.ToList().OrderByDescending(x => x.PublicationDate).Take(3);
        }

        public IEnumerable<News> OrderByDescendingPublicationDateGetAll()
        {
            return _context.Newses.ToList().OrderByDescending(x => x.PublicationDate);
        }
    }
}