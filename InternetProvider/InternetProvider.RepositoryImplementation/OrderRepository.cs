﻿using System.Collections.Generic;
using System.Data.Entity;
using InternetProvider.BusinessObjects;
using InternetProvider.DbContext;
using InternetProvider.RepositoryInterfaces;

namespace InternetProvider.RepositoryImplementation
{
    public class OrderRepository : IRepository<Order>
    {
        private readonly InternetProviderContext _context;

        public OrderRepository(InternetProviderContext dbContext)
        {
            _context = dbContext;
        }

        public IEnumerable<Order> GetAll()
        {
            return _context.Orders;
        }

        public Order Get(int id)
        {
            return _context.Orders.Find(id);
        }

        public void Create(Order item)
        {
            _context.Orders.Add(item);
        }

        public void Update(Order item)
        {
            _context.Entry(item).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            var order = _context.Orders.Find(id);
            if (order != null)
                _context.Orders.Remove(order);
        }
    }
}