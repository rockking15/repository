﻿using System.Collections.Generic;
using System.Data.Entity;
using InternetProvider.BusinessObjects;
using InternetProvider.DbContext;
using InternetProvider.RepositoryInterfaces;

namespace InternetProvider.RepositoryImplementation
{
    public class AdditionalFeatureRepository : IRepository<AdditionalFeature>
    {
        private readonly InternetProviderContext _context;

        public AdditionalFeatureRepository(InternetProviderContext dbContext)
        {
            _context = dbContext;
        }

        public IEnumerable<AdditionalFeature> GetAll()
        {
            return _context.AdditionalFeatures;
        }

        public AdditionalFeature Get(int id)
        {
            return _context.AdditionalFeatures.Find(id);
        }

        public void Create(AdditionalFeature item)
        {
            _context.AdditionalFeatures.Add(item);
        }

        public void Update(AdditionalFeature item)
        {
            _context.Entry(item).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            var additionalFeature = _context.AdditionalFeatures.Find(id);
            if (additionalFeature != null)
                _context.AdditionalFeatures.Remove(additionalFeature);
        }

        public List<AdditionalFeature> GetAdditionalFeatures(int id)
        {
            var account = _context.Accounts.Find(id);

            var additionalFeatures = account.AdditionalFeatures;

            var list = new List<AdditionalFeature>();
            list.AddRange(additionalFeatures);

            return list;
        }
    }
}