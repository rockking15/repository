﻿using System.Collections.Generic;
using System.Data.Entity;
using InternetProvider.BusinessObjects;
using InternetProvider.DbContext;
using InternetProvider.RepositoryInterfaces;

namespace InternetProvider.RepositoryImplementation
{
    public class PictureRepository : IRepository<Picture>
    {
        private readonly InternetProviderContext _context;

        public PictureRepository(InternetProviderContext dbContext)
        {
            _context = dbContext;
        }

        public IEnumerable<Picture> GetAll()
        {
            return _context.Pictures;
        }

        public Picture Get(int id)
        {
            return _context.Pictures.Find(id);
        }

        public void Create(Picture item)
        {
            _context.Pictures.Add(item);
        }

        public void Update(Picture item)
        {
            _context.Entry(item).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            var picture = _context.Pictures.Find(id);
            if (picture != null)
                _context.Pictures.Remove(picture);
        }
    }
}