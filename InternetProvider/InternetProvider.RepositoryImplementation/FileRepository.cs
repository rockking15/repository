﻿using System.Collections.Generic;
using System.Data.Entity;
using InternetProvider.BusinessObjects;
using InternetProvider.DbContext;
using InternetProvider.RepositoryInterfaces;

namespace InternetProvider.RepositoryImplementation
{
    public class FileRepository : IRepository<File>
    {
        private readonly InternetProviderContext _context;

        public FileRepository(InternetProviderContext dbContext)
        {
            _context = dbContext;
        }

        public IEnumerable<File> GetAll()
        {
            return _context.Files;
        }

        public File Get(int id)
        {
            return _context.Files.Find(id);
        }

        public void Create(File item)
        {
            _context.Files.Add(item);
        }

        public void Update(File item)
        {
            _context.Entry(item).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            var file = _context.Files.Find(id);
            if (file != null)
                _context.Files.Remove(file);
        }
    }
}