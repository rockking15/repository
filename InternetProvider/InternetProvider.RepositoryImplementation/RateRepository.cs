﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using InternetProvider.BusinessObjects;
using InternetProvider.DbContext;
using InternetProvider.RepositoryInterfaces;

namespace InternetProvider.RepositoryImplementation
{
    public class RateRepository : IRepository<Rate>
    {
        private readonly InternetProviderContext _context;

        public RateRepository(InternetProviderContext dbContext)
        {
            _context = dbContext;
        }

        public IEnumerable<Rate> GetAll()
        {
            return _context.Rates;
        }

        public Rate Get(int id)
        {
            return _context.Rates.Find(id);
        }

        public void Create(Rate item)
        {
            _context.Rates.Add(item);
        }

        public void Update(Rate item)
        {
            _context.Entry(item).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            var rate = _context.Rates.Find(id);
            if (rate != null)
                _context.Rates.Remove(rate);
        }

        public Rate Get(int? id)
        {
            return _context.Rates.Find(id);
        }

        public List<Rate> GetRates(int id)
        {
            var account = _context.Accounts.Find(id);

            var rates = account.Rates;

            var list = new List<Rate>();
            list.AddRange(rates);

            return list;
        }

        public IEnumerable<Rate> SortByPriceDes(int id)
        {
            return _context.Rates.ToList().Where(y => y.Service.Id == id).OrderByDescending(x => x.Price);
        }

        public IEnumerable<Rate> SortByPriceInc(int id)
        {
            return _context.Rates.ToList().Where(y => y.Service.Id == id).OrderBy(x => x.Price);
        }

        public IEnumerable<Rate> SortByAlph(int id)
        {
            return _context.Rates.ToList().Where(y => y.Service.Id == id).OrderBy(x => x.Name).ToList();
        }

        public IEnumerable<Rate> SortByAlphDec(int id)
        {
            return _context.Rates.ToList().Where(y => y.Service.Id == id).OrderByDescending(x => x.Name);
        }
    }
}