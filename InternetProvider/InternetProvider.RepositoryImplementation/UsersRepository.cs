﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using InternetProvider.BusinessObjects;
using InternetProvider.DbContext;
using InternetProvider.RepositoryInterfaces;

namespace InternetProvider.RepositoryImplementation
{
    public class UsersRepository : IRepository<User>
    {
        private readonly InternetProviderContext _context;

        public UsersRepository(InternetProviderContext dbContext)
        {
            _context = dbContext;
        }

        public IEnumerable<User> GetAll()
        {
            return _context.Users;
        }

        public User Get(int id)
        {
            return _context.Users.Find(id);
        }

        public void Create(User item)
        {
            _context.Users.Add(item);
        }

        public void Update(User item)
        {
            _context.Entry(item).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            var user = _context.Users.Find(id);
            if (user != null)
                _context.Users.Remove(user);
        }

        public User Find(string name)
        {
            var user = (from t in _context.Users
                where t.Email == name
                select t).FirstOrDefault();
            return user;
        }

        public int GetMaxId()
        {
            return _context.Users.OrderByDescending(x => x.Id).First().Id;
        }

        public IEnumerable<User> GetUsersRegistrationToday()
        {
            return
                _context.Users.ToList()
                    .Where(x => x.RegistrationTime.ToShortDateString() == DateTime.Now.ToShortDateString());
        }
    }
}