﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using InternetProvider.BusinessObjects;

namespace InternetProvider.DbContext
{
    public class InternetProviderContext : System.Data.Entity.DbContext
    {
        public InternetProviderContext()
            : base("name=InternetProvider")
        {
        }

        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Service> Services { get; set; }
        public virtual DbSet<Rate> Rates { get; set; }
        public virtual DbSet<Account> Accounts { get; set; }
        public virtual DbSet<AdditionalFeature> AdditionalFeatures { get; set; }
        public virtual DbSet<News> Newses { get; set; }
        public virtual DbSet<Application> Applications { get; set; }
        public virtual DbSet<Map> Maps { get; set; }
        public virtual DbSet<LogEntry> LogEntries { get; set; }
        public virtual DbSet<Payment> Payments { get; set; }
        public virtual DbSet<Comment> Comments { get; set; }
        public virtual DbSet<File> Files { get; set; }
        public virtual DbSet<Advantage> Advantages { get; set; }
        public virtual DbSet<Picture> Pictures { get; set; }
        public virtual DbSet<Order> Orders { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}