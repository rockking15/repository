﻿using System.Collections.Generic;

namespace InternetProvider.BusinessObjects
{
    public class Rate
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public int ServiceId { get; set; }
        public virtual Service Service { get; set; }
        public virtual ICollection<Account> Accounts { get; set; }
        public virtual ICollection<Application> Applications { get; set; }
        public virtual File File { get; set; }
    }
}