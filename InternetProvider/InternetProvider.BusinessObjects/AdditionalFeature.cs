﻿using System.Collections.Generic;

namespace InternetProvider.BusinessObjects
{
    public class AdditionalFeature
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public virtual ICollection<Application> Applications { get; set; }
    }
}