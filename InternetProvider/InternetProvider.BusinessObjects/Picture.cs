﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace InternetProvider.BusinessObjects
{
    public class Picture
    {
        [Key]
        public int Id { get; set; }

        public string PicturePath { get; set; }

        [ForeignKey("Advantage")]
        public int? AdvantageId { get; set; }

        public virtual Advantage Advantage { get; set; }

        [ForeignKey("News")]
        public int? NewsId { get; set; }

        public virtual News News { get; set; }
    }
}