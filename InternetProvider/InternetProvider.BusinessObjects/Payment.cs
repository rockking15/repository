﻿using System;

namespace InternetProvider.BusinessObjects
{
    public class Payment
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public int AccountId { get; set; }
        public decimal Sum { get; set; }
        public virtual Account Account { get; set; }
    }
}