﻿using System.Collections.Generic;

namespace InternetProvider.BusinessObjects
{
    public class Advantage
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Text { get; set; }
        public virtual ICollection<Picture> Picture { get; set; }
    }
}