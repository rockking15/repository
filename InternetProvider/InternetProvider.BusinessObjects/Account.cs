﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace InternetProvider.BusinessObjects
{
    public class Account
    {
        [ForeignKey("User")]
        public int Id { get; set; }

        public decimal Money { get; set; }
        public decimal Debt { get; set; }
        public int UserId { get; set; }

        public virtual User User { get; set; }
        public virtual ICollection<Rate> Rates { get; set; }
        public virtual ICollection<Application> Applications { get; set; }
        public virtual ICollection<Payment> Payments { get; set; }
        public virtual ICollection<AdditionalFeature> AdditionalFeatures { get; set; }
    }
}