﻿namespace InternetProvider.BusinessObjects
{
    public class LogEntry
    {
        public int Id { get; set; }
        public string CallSite { get; set; }
        public string Date { get; set; }
        public string Exception { get; set; }
        public string EventLevel { get; set; }
        public string MachineName { get; set; }
        public string EventMessage { get; set; }
        public string StackTrace { get; set; }
        public string Thread { get; set; }
        public string ErrorSource { get; set; }
        public string ErrorClass { get; set; }
        public string ErrorMethod { get; set; }
        public string ErrorMessage { get; set; }
        public string UserName { get; set; }
    }
}