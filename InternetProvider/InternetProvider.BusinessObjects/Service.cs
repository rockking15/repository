﻿using System.Collections.Generic;

namespace InternetProvider.BusinessObjects
{
    public class Service
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<Rate> Rates { get; set; }
    }
}