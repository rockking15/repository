﻿using System;

namespace InternetProvider.BusinessObjects
{
    public class Comment
    {
        public int Id { get; set; }
        public string AuthorName { get; set; }
        public DateTime Time { get; set; }
        public string UserComm { get; set; }
        public int NewsId { get; set; }
        public virtual News News { get; set; }
    }
}