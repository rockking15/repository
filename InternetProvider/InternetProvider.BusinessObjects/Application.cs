﻿using System;
using System.Collections.Generic;

namespace InternetProvider.BusinessObjects
{
    public class Application
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Adress { get; set; }
        public string Phone { get; set; }
        public DateTime ApplicationTime { get; set; }
        public int AccountId { get; set; }
        public int? RateId { get; set; }
        public string Status { get; set; }
        public virtual Rate Rate { get; set; }
        public virtual Account Account { get; set; }
        public virtual ICollection<AdditionalFeature> AdditionalFeatures { get; set; }
    }
}