﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace InternetProvider.BusinessObjects
{
    public class File
    {
        [Key]
        [ForeignKey("Rate")]
        public int Id { get; set; }

        public string Name { get; set; }
        public int RateId { get; set; }
        public virtual Rate Rate { get; set; }
    }
}