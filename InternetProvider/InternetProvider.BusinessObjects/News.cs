﻿using System;
using System.Collections.Generic;

namespace InternetProvider.BusinessObjects
{
    public class News
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public DateTime PublicationDate { get; set; }
        public string Author { get; set; }
        public virtual ICollection<Picture> Picture { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
    }
}