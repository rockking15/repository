﻿using System;
using System.Data.Entity;
using System.Threading;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using InternetProvider.DbContext;
using InternetProvider.DbInitializer;
using Microsoft.Practices.Unity.Mvc;
using NLog;
using WebMatrix.WebData;

namespace InternetProvider
{
    public class MvcApplication : HttpApplication
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        protected void Application_Start()
        {
            Logger.Info("Application Start");
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            Database.SetInitializer(new Initializer());

            var container = UnityConfig.GetConfiguredContainer();
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));

            var context = new InternetProviderContext();
            context.Database.Initialize(true);
            if (!WebSecurity.Initialized)
                WebSecurity.InitializeDatabaseConnection("InternetProvider", "User", "Id", "Email", true);
        }

        public override void Init()
        {
            Logger.Info("Application Init");
        }

        public override void Dispose()
        {
            Logger.Info("Application Dispose");
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            var ex = Server.GetLastError();
            if (ex is ThreadAbortException)
                return;
            Logger.Error(ex, "Application_Error{0}", ex.Message);
        }

        protected void Application_End(object sender, EventArgs e)
        {
            var shutdownReason = HostingEnvironment.ShutdownReason;
            Logger.Info($"Application End: {shutdownReason}");
        }
    }
}