﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using InternetProvider.BusinessObjects;

namespace InternetProvider.ViewModels
{
    public class RateViewModel
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Name is required.")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Description is required.")]
        public string Description { get; set; }

        [Required(ErrorMessage = "Price is required.")]
        [Range(1, double.PositiveInfinity)]
        public decimal Price { get; set; }

        public List<Service> Services { get; set; }
        public int SelectedService { get; set; }
        public string Status { get; set; }
        public virtual File File { get; set; }
    }
}