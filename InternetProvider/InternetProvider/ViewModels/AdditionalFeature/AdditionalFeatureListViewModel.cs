﻿using System.Collections.Generic;
using InternetProvider.BusinessObjects;

namespace InternetProvider.ViewModels
{
    public class AdditionalFeatureListViewModel
    {
        public List<AdditionalFeature> AdditionalFeatures { get; set; }
    }
}