﻿using System.Collections.Generic;
using InternetProvider.BusinessObjects;

namespace InternetProvider.ViewModels
{
    public class CoveragePointsListViewModel
    {
        public List<Map> Maps { get; set; }
    }
}