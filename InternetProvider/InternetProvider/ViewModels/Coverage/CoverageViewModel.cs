﻿using System.ComponentModel.DataAnnotations;

namespace InternetProvider.ViewModels
{
    public class CoverageViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Geolong is required.")]
        [Range(-180, 180, ErrorMessage = "GeoLong must be between -180 and 180")]
        public double GeoLong { get; set; } // долгота - для карт google

        [Required(ErrorMessage = "GeoLat is required.")]
        [Range(-90, 90, ErrorMessage = "GeoLat must be between -90 and 90")]
        public double GeoLat { get; set; } // широта - для карт google
    }
}