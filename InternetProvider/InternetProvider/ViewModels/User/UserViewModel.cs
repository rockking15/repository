﻿using System.Collections.Generic;
using System.Web.Security;
using InternetProvider.BusinessObjects;

namespace InternetProvider.ViewModels
{
    public class UserViewModel
    {
        public List<MembershipUser> UsersList { get; set; }
        public List<MembershipUser> ModeratorsList { get; set; }
        public List<User> Users { get; set; }
    }
}