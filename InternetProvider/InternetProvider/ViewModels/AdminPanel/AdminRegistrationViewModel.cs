﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using InternetProvider.BusinessObjects;

namespace InternetProvider.ViewModels
{
    public class AdminRegistrationViewModel
    {
        [Required(ErrorMessage = "FirstName is required.")]
        [StringLength(100, ErrorMessage = "The FirstName must be at least 3 characters long.", MinimumLength = 3)]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "LastName is required.")]
        public string LastName { get; set; }

        [DataType(DataType.EmailAddress)]
        [Required(ErrorMessage = "Email is required.")]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage = "Invalid Email")]
        public string Email { get; set; }

        [DataType(DataType.Date)]
        [Column(TypeName = "date")]
        [Required(ErrorMessage = "DateOfBirthday is required.")]
        public DateTime DateOfBirthday { get; set; }

        [Required(ErrorMessage = "Adress is required.")]
        public string Adress { get; set; }

        [Required(ErrorMessage = "Phone is required.")]
        public string Phone { get; set; }

        public int SelectedRate { get; set; }
        public List<int> SelectedAdditionalFeatures { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "The password can not be less than 6 and more than 50 characters",
             MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public DateTime RegistrationTime { get; set; }
        public List<Rate> Rates { get; set; }
        public List<AdditionalFeature> AdditionalFeatures { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string Status { get; set; }
    }
}