﻿using System.Collections.Generic;
using InternetProvider.BusinessObjects;

namespace InternetProvider.ViewModels
{
    public class AdminPanelHomeViewModel
    {
        public List<User> Users { get; set; }
        public List<Comment> Comments { get; set; }
        public List<Application> Applications { get; set; }
    }
}