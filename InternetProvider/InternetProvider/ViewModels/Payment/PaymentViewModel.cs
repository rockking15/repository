﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using InternetProvider.BusinessObjects;

namespace InternetProvider.ViewModels
{
    public class PaymentViewModel
    {
        [Key]
        public string Id { get; set; }

        public DateTime Date { get; set; }

        [Required(ErrorMessage = "AccountNumber is required.")]
        [RegularExpression("([1-9][0-9]*)", ErrorMessage = "Invalid AccountNumber")]
        [Range(1, int.MaxValue)]
        public int AccountId { get; set; }

        [Range(0, double.PositiveInfinity)]
        [Required(ErrorMessage = "Sum is required.")]
        public decimal Sum { get; set; }

        public int SelectedVal { get; set; }
        public string Val { get; set; }
        public virtual Account Account { get; set; }
    }
}