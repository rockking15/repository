﻿using System.Collections.Generic;
using InternetProvider.BusinessObjects;

namespace InternetProvider.ViewModels
{
    public class HomeViewModel
    {
        public List<News> Newses { get; set; }
        public List<Advantage> Advantages { get; set; }
    }
}