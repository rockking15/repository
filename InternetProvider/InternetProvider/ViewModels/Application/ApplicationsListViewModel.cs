﻿using System.Collections.Generic;
using InternetProvider.BusinessObjects;

namespace InternetProvider.ViewModels
{
    public class ApplicationsListViewModel
    {
        public List<Application> Applications { get; set; }
        public int AccountId { get; set; }
    }
}