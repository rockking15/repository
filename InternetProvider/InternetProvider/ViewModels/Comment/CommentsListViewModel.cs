﻿using System.Collections.Generic;
using InternetProvider.BusinessObjects;

namespace InternetProvider.ViewModels
{
    public class CommentsListViewModel
    {
        public List<Comment> Comments { get; set; }
    }
}