﻿using System;
using System.ComponentModel.DataAnnotations;

namespace InternetProvider.ViewModels
{
    public class CommentViewModel
    {
        public int Id { get; set; }

        [Required]
        public string AuthorName { get; set; }

        public DateTime Time { get; set; }

        [Required]
        public string UserComm { get; set; }

        public int NewsId { get; set; }
    }
}