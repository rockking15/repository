﻿using System.Collections.Generic;
using InternetProvider.BusinessObjects;

namespace InternetProvider.ViewModels
{
    public class AccountListViewModel
    {
        public List<Account> Accounts { get; set; }
    }
}