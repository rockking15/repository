﻿using System.Collections.Generic;
using InternetProvider.BusinessObjects;

namespace InternetProvider.ViewModels
{
    public class ServiceListViewModel
    {
        public List<Service> Services { get; set; }
    }
}