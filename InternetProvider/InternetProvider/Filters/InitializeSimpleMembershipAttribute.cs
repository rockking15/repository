﻿using System;
using System.Web.Mvc;
using System.Web.Security;
using WebMatrix.WebData;

namespace InternetProvider.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public sealed class InitializeSimpleMembershipAttribute : ActionFilterAttribute
    {
        public void SimpleMembershipInitializer()
        {
            WebSecurity.InitializeDatabaseConnection("InternetProvider", "User", "Id", "Email", true);

            var roles = (SimpleRoleProvider) Roles.Provider;
            var membership = (SimpleMembershipProvider) Membership.Provider;

            if (!roles.RoleExists("Admin"))
                roles.CreateRole("Admin");

            if (!roles.RoleExists("Moderator"))
                roles.CreateRole("Moderator");

            if (membership.GetUser("admin", false) == null)

            {
                WebSecurity.CreateUserAndAccount("admin@mail.ru", "123123",
                    new
                    {
                        FirstName = "admin",
                        LastName = "admin",
                        DateOfBirthday = "1990-01-05",
                        Adress = "Ukraine",
                        Phone = "2281488",
                        RegistrationTime = DateTime.Now.AddYears(-5)
                    });

                roles.AddUsersToRoles(new[] {"admin@mail.ru"}, new[] {"Admin"});
            }

            if (membership.GetUser("moderator", false) == null)

            {
                WebSecurity.CreateUserAndAccount("moderator@gmail.com", "123123",
                    new
                    {
                        FirstName = "moder",
                        LastName = "moder",
                        DateOfBirthday = "2000-03-05",
                        Adress = "Kharkiv",
                        Phone = "125521",
                        RegistrationTime = DateTime.Now.AddYears(-3)
                    });

                roles.AddUsersToRoles(new[] {"moderator@gmail.com"}, new[] {"Moderator"});
            }
        }
    }
}