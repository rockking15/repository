﻿using System.Data.Common;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web.Mvc;
using InternetProvider.BusinessObjects;
using InternetProvider.RepositoryImplementation;
using InternetProvider.ViewModels;
using NLog;

namespace InternetProvider.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdditionalFeaturesController : Controller
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly UnitOfWork _unitOfWork;

        public AdditionalFeaturesController(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public ViewResult Index()
        {
            Logger.Trace("Starting AdditionalFeaturesController Index method. Previous page {0}",
                Request.UrlReferrer);
            return View();
        }

        public PartialViewResult Create()
        {
            Logger.Trace("Starting AdditionalFeaturesController  Create method. Previous page {0}",
                Request.UrlReferrer);
            ViewBag.Message = TempData["message"] as string;
            var model = new AdditionalFeatureViewModel();
            return PartialView(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(AdditionalFeature model)
        {
            Logger.Trace("Starting AdditionalFeaturesController Save method. Previous page {0}",
                Request.UrlReferrer);
            if (ModelState.IsValid)
            {
                var additionalFeature = new AdditionalFeature
                {
                    Id = model.Id,
                    Name = model.Name,
                    Price = model.Price
                };

                try
                {
                    _unitOfWork.AdditionalFeature.Create(additionalFeature);
                    _unitOfWork.Save();
                }
                catch (DbUpdateException ex)
                {
                    Logger.Error("DbUpdateException {0}", ex.Message);
                    return View("~/Views/Error/ErrorPage.cshtml",
                        new HandleErrorInfo(ex, ControllerContext.RouteData.Values["controller"].ToString(),
                            ControllerContext.RouteData.Values["action"].ToString()));
                }

                Logger.Info("Additional feature {0} created successfully", model.Name);
                TempData["message"] = "Additional Feature created successfully";
                return RedirectToAction("Create", "AdditionalFeatures");
            }
            Logger.Info("Additional service {0} not created", model.Name);
            return RedirectToAction("Create", "AdditionalFeatures");
        }

        public ViewResult AdditionalFeaturesList()
        {
            Logger.Trace("Starting AdditionalFeaturesController AdditionalFeaturesList method. Previous page {0}",
                Request.UrlReferrer);
            ViewBag.Message = TempData["message"] as string;
            var model = new AdditionalFeatureListViewModel
            {
                AdditionalFeatures = _unitOfWork.AdditionalFeature.GetAll().ToList()
            };
            return View(model);
        }

        public ViewResult AdditionalFeaturesListDelete()
        {
            Logger.Trace(
                "Starting AdditionalFeaturesController AdditionalFeaturesListDelete method. Previous page {0}",
                Request.UrlReferrer);
            ViewBag.Message = TempData["message"] as string;
            var model = new AdditionalFeatureListViewModel
            {
                AdditionalFeatures = _unitOfWork.AdditionalFeature.GetAll().ToList()
            };
            return View(model);
        }

        public PartialViewResult Update(int id)
        {
            Logger.Trace(
                "Running AdditionalFeaturesController Update method with the parameter id = {0}. Previous page {1}", id,
                Request.UrlReferrer);
            Logger.Trace("Searching AdditionalFeature with Id={0}", id);
            var additionalFeature = _unitOfWork.AdditionalFeature.Get(id);
            var model = new AdditionalFeatureViewModel
            {
                Id = additionalFeature.Id,
                Name = additionalFeature.Name,
                Price = additionalFeature.Price
            };
            return PartialView(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(AdditionalFeature model)
        {
            Logger.Trace("Running AdditionalFeaturesController Create method. Previous page {0}",
                Request.UrlReferrer);
            if (ModelState.IsValid)
            {
                var additionalFeature = new AdditionalFeature
                {
                    Id = model.Id,
                    Name = model.Name,
                    Price = model.Price
                };

                try
                {
                    _unitOfWork.AdditionalFeature.Update(additionalFeature);
                    _unitOfWork.Save();
                }
                catch (DbUpdateException ex)
                {
                    Logger.Error("DbUpdateException {0}", ex.Message);
                    return View("~/Views/Error/ErrorPage.cshtml",
                        new HandleErrorInfo(ex, ControllerContext.RouteData.Values["controller"].ToString(),
                            ControllerContext.RouteData.Values["action"].ToString()));
                }

                Logger.Info("Additional service {0} successfully edited", model.Name);
                TempData["message"] = "Additional Feature successfully edited";
                return RedirectToAction("AdditionalFeaturesList", "AdditionalFeatures");
            }
            Logger.Info("Additional Feature {0}  have not been edited", model.Name);
            TempData["message"] = "Element not edited";
            return RedirectToAction("AdditionalFeaturesList", "AdditionalFeatures");
        }

        public ActionResult Delete(int id)
        {
            Logger.Trace(
                "Running AdditionalFeaturesController Update method with the parameter id = {0}. Previous {1}", id,
                Request.UrlReferrer);
            Logger.Trace("Searching AdditionalFeature with Id={0}", id);
            var additionalFeature = _unitOfWork.AdditionalFeature.Get(id);
            _unitOfWork.AdditionalFeature.Delete(id);
            try
            {
                _unitOfWork.Save();
            }
            catch (DbException ex)
            {
                Logger.Error("DbException {0}", ex.Message);
                return View("~/Views/Error/ErrorPage.cshtml",
                    new HandleErrorInfo(ex, ControllerContext.RouteData.Values["controller"].ToString(),
                        ControllerContext.RouteData.Values["action"].ToString()));
            }

            Logger.Info("Additional feature {0} has been removed", additionalFeature.Name);
            TempData["message"] = "Additional Feature successfully deleted";
            return RedirectToAction("AdditionalFeaturesListDelete", "AdditionalFeatures");
        }
    }
}