﻿using System;
using System.Data.Common;
using System.Linq;
using System.Web.Mvc;
using InternetProvider.BusinessObjects;
using InternetProvider.RepositoryImplementation;
using InternetProvider.ViewModels;
using NLog;

namespace InternetProvider.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ApplicationController : Controller
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly UnitOfWork _unitOfWork;

        public ApplicationController(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public ViewResult Index()
        {
            Logger.Trace("Starting ApplicationController Index method. Previous page {0}", Request.UrlReferrer);
            ViewBag.Error = TempData["error"] as string;
            var model = new ApplicationsListViewModel
            {
                Applications = _unitOfWork.Application.GetApplicationsSortByTimeOld().ToList()
            };
            return View(model);
        }

        public ActionResult Execute(int id)
        {
            Logger.Trace("Starting ApplicationController Execute method with the parameter id = {0}. Previous {1}", id,
                Request.UrlReferrer);
            var application = _unitOfWork.Application.Get(id);
            var account = _unitOfWork.Account.Get(application.AccountId);

            var additionalFeturesSumPrice = application.AdditionalFeatures.Sum(x => x.Price);

            Logger.Info("Searching and execution application with Id={0}", id);
            if (application.RateId != null)
            {
                var rate = _unitOfWork.Rate.Get(application.RateId);
                if (account.Money >= rate.Price + additionalFeturesSumPrice)
                {
                    account.Money -= rate.Price + additionalFeturesSumPrice;
                    Logger.Info("Removing money from the user's account Id={0}", account.Id);
                }
                else
                {
                    Logger.Info("User {0} don`t have enough money in the account", account.Id);
                    TempData["error"] = "User don`t have enough money in the account";
                    return RedirectToAction("Index");
                }
                var order = new Order
                {
                    ApplicationTime = DateTime.Now,
                    AccountId = application.AccountId,
                    RateId = (int) application.RateId
                };
                _unitOfWork.Order.Create(order);
                Logger.Info("Adding a new user to the system {0}", application.AccountId);
            }
            else
            {
                if (account.Money >= additionalFeturesSumPrice)
                {
                    account.Money -= additionalFeturesSumPrice;
                    Logger.Info("Removing money from the user's account Id={0}", account.Id);
                }
                else
                {
                    Logger.Info("User {0} don`t have enough money in the account", account.Id);
                    TempData["error"] = "User don`t have enough money in the account";
                    return RedirectToAction("Index");
                }
            }
            application.Status = "Done";
            Logger.Info("Change order status {0}", application.Id);
            try
            {
                _unitOfWork.Save();
            }
            catch (DbException ex)
            {
                Logger.Error("DbException {0}", ex.Message);
                return View("~/Views/Error/ErrorPage.cshtml",
                    new HandleErrorInfo(ex, ControllerContext.RouteData.Values["controller"].ToString(),
                        ControllerContext.RouteData.Values["action"].ToString()));
            }

            return RedirectToAction("Index");
        }

        public ViewResult SortByTime()
        {
            Logger.Trace("Starting ApplicationController SortByTime method. Previous page {0}", Request.UrlReferrer);
            var model = new ApplicationsListViewModel
            {
                Applications = _unitOfWork.Application.GetApplicationsSortByTime().ToList()
            };
            return View("Index", model);
        }

        public ViewResult SortByTimeOld()
        {
            Logger.Trace("Starting ApplicationController SortByTimeOld method. Previous page {0}",
                Request.UrlReferrer);
            var model = new ApplicationsListViewModel
            {
                Applications = _unitOfWork.Application.GetApplicationsSortByTimeOld().ToList()
            };
            return View("Index", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SearchByAccountNumber(ApplicationsListViewModel vmodel)
        {
            Logger.Trace("Starting ApplicationController SearchByAccountNumber method. Previous page {0}",
                Request.UrlReferrer);

            if (ModelState.IsValid)
            {
                var model = new ApplicationsListViewModel
                {
                    Applications = _unitOfWork.Application.GetAll().Where(x => x.AccountId == vmodel.AccountId).ToList()
                };
                if (model.Applications.Count == 0)
                {
                    TempData["error"] = "Account does not exist or it has no orders";
                    return RedirectToAction("Index");
                }
                return View("Index", model);
            }

            return RedirectToAction("Index");
        }
    }
}