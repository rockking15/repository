﻿using System.Data.Common;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web.Mvc;
using InternetProvider.BusinessObjects;
using InternetProvider.RepositoryImplementation;
using InternetProvider.ViewModels;
using NLog;

namespace InternetProvider.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class CoverageController : Controller
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly UnitOfWork _unitOfWork;

        public CoverageController(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public ViewResult Index()
        {
            Logger.Trace("Starting CoverageController Index method. Previous page {0}", Request.UrlReferrer);
            return View();
        }

        public PartialViewResult Create()
        {
            Logger.Trace("Starting CoverageController Create method. Previous page {0}", Request.UrlReferrer);
            ViewBag.Message = TempData["message"] as string;
            var model = new CoverageViewModel();
            return PartialView(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(CoverageViewModel model)
        {
            Logger.Trace("Starting CoverageController Save method. Previous page {0}", Request.UrlReferrer);
            if (ModelState.IsValid)
            {
                var map = new Map
                {
                    Id = model.Id,
                    GeoLat = model.GeoLat,
                    GeoLong = model.GeoLong
                };
                try
                {
                    _unitOfWork.Map.Create(map);
                    _unitOfWork.Save();
                }
                catch (DbException ex)
                {
                    Logger.Error("DbException {0}", ex.Message);
                    return View("~/Views/Error/ErrorPage.cshtml",
                        new HandleErrorInfo(ex, ControllerContext.RouteData.Values["controller"].ToString(),
                            ControllerContext.RouteData.Values["action"].ToString()));
                }

                Logger.Info("Creating a new mark on the map Id={0}", model.Id);
                TempData["message"] = "Point created successfully";
                return RedirectToAction("Create", "Coverage");
            }
            Logger.Info("Mark on the map can not be created Geolat ={0} , Geolong={1}", model.GeoLat, model.GeoLong);
            return RedirectToAction("Create", "Coverage");
        }

        public ViewResult CoveragePointsList()
        {
            Logger.Trace("Starting CoverageController CoveragePointsList method. Previous page {0}",
                Request.UrlReferrer);
            ViewBag.Message = TempData["message"] as string;
            var model = new CoveragePointsListViewModel {Maps = _unitOfWork.Map.GetAll().ToList()};
            return View(model);
        }

        public ViewResult CoveragePointsListDelete()
        {
            Logger.Trace("Starting CoverageController CoveragePointsListDelete method. Previous page {0}",
                Request.UrlReferrer);
            ViewBag.Message = TempData["message"] as string;
            var model = new CoveragePointsListViewModel {Maps = _unitOfWork.Map.GetAll().ToList()};
            return View(model);
        }

        public PartialViewResult Update(int id)
        {
            Logger.Trace("starting CoverageController Update method with the parameter id = {0}. Previous {1}", id,
                Request.UrlReferrer);
            var map = _unitOfWork.Map.Get(id);
            Logger.Trace("Update Point with Id={0}", id);
            var model = new CoverageViewModel
            {
                GeoLat = map.GeoLat,
                GeoLong = map.GeoLong,
                Id = map.Id
            };
            return PartialView(model);
        }

        public ActionResult Delete(int id)
        {
            Logger.Trace("Starting CoverageController Delete method with the parameter id = {0}. Previous {1}", id,
                Request.UrlReferrer);
            var map = _unitOfWork.Map.Get(id);
            Logger.Trace("Delete Point with Id={0}", id);
            try
            {
                _unitOfWork.Map.Delete(id);
                _unitOfWork.Save();
            }
            catch (DbException ex)
            {
                Logger.Error("DbException {0}", ex.Message);
                return View("~/Views/Error/ErrorPage.cshtml",
                    new HandleErrorInfo(ex, ControllerContext.RouteData.Values["controller"].ToString(),
                        ControllerContext.RouteData.Values["action"].ToString()));
            }

            Logger.Info("Mark on the map successfully deleted Geolat={0} , Geolong={1}", map.GeoLat, map.GeoLong);
            TempData["message"] = "Mark on the map successfully deleted";
            return RedirectToAction("CoveragePointsListDelete", "Coverage");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CoverageViewModel model)
        {
            Logger.Trace("Starting CoverageController Create method. Previous page {0}", Request.UrlReferrer);
            if (ModelState.IsValid)
            {
                var map = new Map
                {
                    Id = model.Id,
                    GeoLat = model.GeoLat,
                    GeoLong = model.GeoLong
                };

                try
                {
                    _unitOfWork.Map.Update(map);
                    _unitOfWork.Save();
                }
                catch (DbUpdateException ex)
                {
                    Logger.Error("DbUpdateException {0}", ex.Message);
                    return View("~/Views/Error/ErrorPage.cshtml",
                        new HandleErrorInfo(ex, ControllerContext.RouteData.Values["controller"].ToString(),
                            ControllerContext.RouteData.Values["action"].ToString()));
                }

                Logger.Info("Editing markers on the map  with Id={0}", model.Id);
                TempData["message"] = "Mark on the map successfully edited";
                return RedirectToAction("CoveragePointsList", "Coverage");
            }
            Logger.Info("The mark on the map is not edited  Id={0}", model.Id);
            TempData["message"] = "Element not edited";
            return RedirectToAction("CoveragePointsList", "Coverage");
        }
    }
}