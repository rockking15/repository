﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using InternetProvider.RepositoryImplementation;
using InternetProvider.ViewModels.Reports;

namespace InternetProvider.Areas.Admin.Controllers
{
    public class ReportsController : Controller
    {
        private readonly UnitOfWork _unitOfWork;

        public ReportsController(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public ActionResult Index()
        {
            ReportsViewModel model = new ReportsViewModel();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ShowReportsByDate(ReportsViewModel viewModel)
        {
            var dateTo = Convert.ToDateTime(viewModel.DateTo.ToString("yyyy-MM-dd"));
            var dateFrom = Convert.ToDateTime(viewModel.DateFrom.ToString("yyyy-MM-dd"));
            if (ModelState.IsValid)
            {
                ReportsViewModel model = new ReportsViewModel
                {
                    Applications = _unitOfWork.Application.GetAll()
                        .Where(x => x.ApplicationTime > dateFrom && x.ApplicationTime <= dateTo)
                        .ToList(),
                    Users = _unitOfWork.User.GetAll()
                        .Where(x => x.RegistrationTime > dateFrom && x.RegistrationTime <= dateTo)
                        .ToList()
                };
                return View("Index",model);
            }
            return RedirectToAction("Index");
        }
    }
}