﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using InternetProvider.BusinessLogic;
using InternetProvider.BusinessObjects;
using InternetProvider.RepositoryImplementation;
using InternetProvider.ViewModels;
using NLog;
using WebMatrix.WebData;

namespace InternetProvider.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class UsersController : Controller
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly UnitOfWork _unitOfWork;

        public UsersController(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public ViewResult Index()
        {
            Logger.Trace("Starting UsersController Index method. Previous page {0}", Request.UrlReferrer);
            return View();
        }

        public ViewResult Registration()
        {
            Logger.Trace("Starting UsersController Registration method. Previous page {0}", Request.UrlReferrer);
            ViewBag.Message = TempData["message"] as string;
            var model = new AdminRegistrationViewModel
            {
                RegistrationTime = DateTime.Now,
                Rates = _unitOfWork.Rate.GetAll().ToList(),
                AdditionalFeatures = _unitOfWork.AdditionalFeature.GetAll().ToList()
            };
            return View(model);
        }

        public ContentResult GeneratePassword()
        {
            Logger.Trace("Starting UsersController GeneratePassword method. Previous page {0}", Request.UrlReferrer);
            var userService = new UserService();
            return Content(UserService.GetPassword());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveUser(AdminRegistrationViewModel model)
        {
            Logger.Trace("Starting UsersController SaveUser method. Previous page {0}", Request.UrlReferrer);
            var rate = _unitOfWork.Rate.Get(model.SelectedRate);
            var additionalFeatures = new List<AdditionalFeature>();
            if (model.SelectedAdditionalFeatures != null)
                additionalFeatures.AddRange(
                    model.SelectedAdditionalFeatures.Select(id => _unitOfWork.AdditionalFeature.Get(id)));
            if (ModelState.IsValid)
                try
                {
                    WebSecurity.CreateUserAndAccount(model.Email, model.Password,
                        new
                        {
                            model.FirstName,
                            model.DateOfBirthday,
                            model.LastName,
                            model.Adress,
                            model.Phone,
                            model.RegistrationTime
                        });

                    Logger.Info("User {0} successfully registered", model.Email);
                    Roles.AddUsersToRole(new[] {model.Email}, "User");
                    Logger.Info("The user is added to the role {0}", "User");

                    var account = new Account
                    {
                        Id = _unitOfWork.User.GetMaxId(),
                        Money = 0,
                        Rates = new List<Rate> {rate},
                        AdditionalFeatures = additionalFeatures,
                        Debt = 0
                    };
                    Logger.Info("User Account {0} successfully created", model.Email);
                    var order = new Order
                    {
                        AccountId = account.Id,
                        ApplicationTime = DateTime.Now,
                        RateId = model.SelectedRate
                    };
                    try
                    {
                        _unitOfWork.Account.Create(account);
                        _unitOfWork.Order.Create(order);
                        _unitOfWork.Save();
                    }
                    catch (DbException ex)
                    {
                        Logger.Error("DbException {0}", ex.Message);
                        return View("~/Views/Error/ErrorPage.cshtml",
                            new HandleErrorInfo(ex, ControllerContext.RouteData.Values["controller"].ToString(),
                                ControllerContext.RouteData.Values["action"].ToString()));
                    }

                    Logger.Info("User {0}  and Order {1} successfully created", account.Id, order.Id);
                    TempData["message"] = "User created successfully";
                    return RedirectToAction("Registration", "Users");
                }
                catch (MembershipCreateUserException e)
                {
                    Logger.Error("Error creating user in Membership  {0}", e.Message);
                    ModelState.AddModelError("", e.StatusCode.ToString());
                    return View("~/Views/Error/ErrorPage.cshtml",
                        new HandleErrorInfo(e, ControllerContext.RouteData.Values["controller"].ToString(),
                            ControllerContext.RouteData.Values["action"].ToString()));
                }
            Logger.Info("User {0} is not created", model.Email);
            return View("Registration", model);
        }

        public ViewResult Block()
        {
            Logger.Trace("Starting UsersController Block method. Previous page {0}", Request.UrlReferrer);
            ViewBag.Message = TempData["message"] as string;
            var model = new UserViewModel();
            var usersList = new List<User>();
            foreach (var user in _unitOfWork.User.GetAll())
            {
                var rolesForUser = Roles.GetRolesForUser(user.Email);
                if (rolesForUser.Contains("User") || rolesForUser.Contains("Moderator"))
                    usersList.Add(user);
            }
            model.Users = usersList;
            return View(model);
        }

        public ActionResult BlockSuccess(int id)
        {
            Logger.Trace("Starting UsersController BlockSuccess method parameter id = {0}. Previous page {1}", id,
                Request.UrlReferrer);
            Logger.Trace("Searching Account with Id={0}", id);
            var account = _unitOfWork.Account.Get(id);
            var rolesForUser = Roles.GetRolesForUser(account.User.Email);

            if (rolesForUser.Contains("User"))
                Roles.RemoveUserFromRole(account.User.Email, "User");
            if (rolesForUser.Contains("Moderator"))
                Roles.RemoveUserFromRole(account.User.Email, "Moderator");
            try
            {
                _unitOfWork.Save();
            }
            catch (DbException ex)
            {
                Logger.Error("DbException {0}", ex.Message);
                return View("~/Views/Error/ErrorPage.cshtml",
                    new HandleErrorInfo(ex, ControllerContext.RouteData.Values["controller"].ToString(),
                        ControllerContext.RouteData.Values["action"].ToString()));
            }

            Logger.Info("Account {0} Blocked", account.Id);
            TempData["message"] = "Account Blocked!";
            return RedirectToAction("Block", "Users");
        }

        public ActionResult UnblockSuccess(int id)
        {
            Logger.Trace("Starting UsersController UnblockSuccess method parameter id = {0}. Previous page {1}", id,
                Request.UrlReferrer);
            Logger.Trace("Searching Account with Id={0}", id);
            var account = _unitOfWork.Account.Get(id);
            Roles.AddUserToRole(account.User.Email, "User");
            try
            {
                _unitOfWork.Save();
            }
            catch (DbException ex)
            {
                Logger.Error("DbException {0}", ex.Message);
                return View("~/Views/Error/ErrorPage.cshtml",
                    new HandleErrorInfo(ex, ControllerContext.RouteData.Values["controller"].ToString(),
                        ControllerContext.RouteData.Values["action"].ToString()));
            }

            Logger.Info("Account {0} UnBlocked", account.Id);
            TempData["message"] = "Account UnBlocked!";
            return RedirectToAction("UnBlock", "Users");
        }

        public ViewResult UnBlock()
        {
            Logger.Trace("Starting UsersController Unblock method. Previous page {0}", Request.UrlReferrer);
            ViewBag.Message = TempData["message"] as string;
            var model = new UserViewModel();
            var usersList = new List<User>();
            foreach (var user in _unitOfWork.User.GetAll())
            {
                var rolesForUser = Roles.GetRolesForUser(user.Email);
                if (rolesForUser.Length == 0)
                    usersList.Add(user);
            }
            model.Users = usersList;
            return View(model);
        }

        public ViewResult UsersList()
        {
            Logger.Trace("Starting UsersController UsersList method. Previous page {0}", Request.UrlReferrer);
            var model = new UserViewModel
            {
                UsersList = Roles.GetUsersInRole("User").Select(Membership.GetUser).ToList(),
                ModeratorsList = Roles.GetUsersInRole("Moderator").Select(Membership.GetUser).ToList()
            };

            return View(model);
        }

        public ActionResult AddRole(string username)
        {
            Logger.Trace("Starting UsersController AddRole method with parameter Username = {0}. Previous page {1}",
                username, Request.UrlReferrer);
            Roles.AddUserToRole(username, "Moderator");
            Roles.RemoveUserFromRole(username, "User");
            Logger.Info("User {0} added a moderator role", username);
            return RedirectToAction("UsersList", "Users");
        }

        public ActionResult RemoveRole(string username)
        {
            Logger.Trace("Starting UsersController RemoveRole method with parameter Username = {0}. Previous page {1}",
                username, Request.UrlReferrer);
            Roles.AddUserToRole(username, "User");
            Roles.RemoveUserFromRole(username, "Moderator");
            Logger.Info("The User {0} removed a moderator role", username);
            return RedirectToAction("UsersList", "Users");
        }

        public ViewResult UpdateUser()
        {
            Logger.Trace("Starting UsersController UpdateUser method. Previous page {0}", Request.UrlReferrer);
            var model = new UserViewModel {Users = _unitOfWork.User.GetAll().ToList()};
            ViewBag.Message = TempData["message"] as string;
            return View(model);
        }

        public PartialViewResult EditAndUpdate(int id)
        {
            Logger.Trace("Starting UsersController EditAndUpdate method with parameter Id = {0}. Previous page {1}",
                id, Request.UrlReferrer);
            Logger.Trace("Searching User with Id={0}", id);
            var user = _unitOfWork.User.Get(id);
            var model = new UpdateUserViewModel
            {
                Email = user.Email,
                Id = user.Id,
                Adress = user.Adress,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Phone = user.Phone,
                Rates = user.Account.Rates.ToList(),
                AllRates = _unitOfWork.Rate.GetAll().ToList(),
                AccountId = user.Account.Id,
                DateOfBirthday = user.DateOfBirthday,
                RegistrationTime = user.RegistrationTime
            };
            return PartialView(model);
        }

        public ActionResult SaveEditingUser(UpdateUserViewModel model)
        {
            Logger.Trace("Starting UsersController SaveEditingUser method. Previous page {0}", Request.UrlReferrer);
            var ratesList = new List<Rate>();
            if (model.Rates != null)
                ratesList.AddRange(model.Rates.Select(a => _unitOfWork.Rate.GetAll().FirstOrDefault(x => x.Id == a.Id)));

            if (ModelState.IsValid)
            {
                var user = new User
                {
                    Id = model.Id,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    Adress = model.Adress,
                    Phone = model.Phone,
                    Email = model.Email,
                    DateOfBirthday = model.DateOfBirthday,
                    RegistrationTime = model.RegistrationTime
                };

                var account = new Account
                {
                    Id = model.AccountId,
                    Rates = ratesList,
                    UserId = user.Id
                };

                try
                {
                    _unitOfWork.User.Update(user);
                    _unitOfWork.Account.Update(account);
                    _unitOfWork.Save();
                }
                catch (DbUpdateException ex)
                {
                    Logger.Error("DbUpdateException {0}", ex.Message);
                    return View("~/Views/Error/ErrorPage.cshtml",
                        new HandleErrorInfo(ex, ControllerContext.RouteData.Values["controller"].ToString(),
                            ControllerContext.RouteData.Values["action"].ToString()));
                }
                Logger.Info("User {0} successfully edited", model.Email);
                TempData["message"] = "User successfully edited";
                return RedirectToAction("UpdateUser", "Users");
            }
            Logger.Info("User {0} not edited", model.Email);
            TempData["message"] = "User not edited";
            return RedirectToAction("UpdateUser", "Users");
        }

        public ActionResult DeleteRate(int id, int userId)
        {
            Logger.Trace("Starting UsersController DeleteRate method. Previous page {0}", Request.UrlReferrer);
            var user = _unitOfWork.User.Get(userId);
            Logger.Trace("Searching User with Id={0}", userId);
            Logger.Trace("Searching Rate with Id={0}", id);
            foreach (var rate in user.Account.Rates.ToList().Where(rate => rate.Id == id))
                try
                {
                    user.Account.Rates.Remove(rate);
                    _unitOfWork.Save();
                }
                catch (DbException ex)
                {
                    Logger.Error("DbException {0}", ex.Message);
                    return View("~/Views/Error/ErrorPage.cshtml",
                        new HandleErrorInfo(ex, ControllerContext.RouteData.Values["controller"].ToString(),
                            ControllerContext.RouteData.Values["action"].ToString()));
                }
            Logger.Info("In UserAccount {0} successfully removed Rate with Id={1}", userId, id);
            return RedirectToAction("EditAndUpdate", "Users", new {id = userId});
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddRate(UpdateUserViewModel model)
        {
            Logger.Trace("Starting UsersController AddRate method. Previous page {0}", Request.UrlReferrer);
            var user = _unitOfWork.User.Get(model.Id);
            Logger.Trace("Searching User with Id={0}", model.Id);
            foreach (var rate in _unitOfWork.Rate.GetAll().ToList().Where(a => a.Id == model.SelectedRate))
                try
                {
                    user.Account.Rates.Add(rate);
                    _unitOfWork.Save();
                }
                catch (DbException ex)
                {
                    Logger.Error("DbException {0}", ex.Message);
                    return View("~/Views/Error/ErrorPage.cshtml",
                        new HandleErrorInfo(ex, ControllerContext.RouteData.Values["controller"].ToString(),
                            ControllerContext.RouteData.Values["action"].ToString()));
                }
            Logger.Info("In UserAccount {0} successfully Add Rate");
            return RedirectToAction("UpdateUser", "Users");
        }
    }
}