﻿using System.Data.Entity.Infrastructure;
using System.Web.Mvc;
using InternetProvider.BusinessObjects;
using InternetProvider.RepositoryImplementation;
using InternetProvider.ViewModels;
using NLog;

namespace InternetProvider.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin,Moderator")]
    public class ProfileController : Controller
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly UnitOfWork _unitOfWork;

        public ProfileController(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public ViewResult Index()
        {
            Logger.Trace("Starting ProfileController Index method. Previous page {0}", Request.UrlReferrer);
            var user = _unitOfWork.User.Find(User.Identity.Name);
            var model = new PersonalCabinetViewModel
            {
                Id = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Adress = user.Adress,
                Phone = user.Phone,
                DateOfBirthday = user.DateOfBirthday,
                Email = user.Email
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveData(PersonalCabinetViewModel model)
        {
            Logger.Trace("Starting ProfileController SaveData method. Previous page {0}", Request.UrlReferrer);
            if (ModelState.IsValid)
            {
                var user = new User
                {
                    Id = model.Id,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    Adress = model.Adress,
                    Phone = model.Phone,
                    DateOfBirthday = model.DateOfBirthday,
                    Email = model.Email
                };
                try
                {
                    _unitOfWork.User.Update(user);
                    _unitOfWork.Save();
                }
                catch (DbUpdateException ex)
                {
                    Logger.Error("DbUpdateException {0}", ex.Message);
                    return View("~/Views/Error/ErrorPage.cshtml",
                        new HandleErrorInfo(ex, ControllerContext.RouteData.Values["controller"].ToString(),
                            ControllerContext.RouteData.Values["action"].ToString()));
                }

                Logger.Info("User data {0} edited", model.Email);
                ViewBag.Ok = "Data saved successfully";
                return View("Index", model);
            }
            Logger.Info("User Profile {0} is not edited", model.Email);
            return View("Index", model);
        }
    }
}