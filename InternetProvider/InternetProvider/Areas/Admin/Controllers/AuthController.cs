﻿using System.Web.Mvc;
using System.Web.Security;
using InternetProvider.RepositoryImplementation;
using InternetProvider.ViewModels;
using NLog;

namespace InternetProvider.Areas.Admin.Controllers
{
    public class AuthController : Controller
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly UnitOfWork _unitOfWork;

        public AuthController(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [AllowAnonymous]
        [HttpGet]
        public ViewResult Index()
        {
            Logger.Trace("Starting AuthController Index method. Previous page {0}", Request.UrlReferrer);
            var model = new LogInViewModel();
            ViewBag.Error = TempData["error"] as string;
            return View(model);
        }

        public ActionResult LogOff()
        {
            Logger.Trace("Starting AuthController LogOff method. Previous page {0}", Request.UrlReferrer);
            var user = _unitOfWork.User.Find(User.Identity.Name);
            Logger.Info("User {0} logged out", user.Email);
            FormsAuthentication.SignOut();
            return RedirectToAction("Index");
        }
    }
}