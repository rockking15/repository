﻿using System.Data.Common;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web.Mvc;
using InternetProvider.BusinessObjects;
using InternetProvider.RepositoryImplementation;
using InternetProvider.ViewModels;
using NLog;

namespace InternetProvider.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ServicesController : Controller
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly UnitOfWork _unitOfWork;

        public ServicesController(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public ViewResult Index()
        {
            Logger.Trace("Starting ServicesController Index method. Previous page {0}", Request.UrlReferrer);
            return View();
        }

        public PartialViewResult Create()
        {
            Logger.Trace("Starting ServicesController Create method. Previous page {0}", Request.UrlReferrer);
            ViewBag.Message = TempData["message"] as string;
            var model = new ServiceViewModel();
            return PartialView(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(ServiceViewModel model)
        {
            Logger.Trace("Starting ServicesController Save method. Previous page {0}", Request.UrlReferrer);
            if (ModelState.IsValid)
            {
                var service = new Service
                {
                    Id = model.Id,
                    Name = model.Name
                };

                try
                {
                    _unitOfWork.Service.Create(service);
                    _unitOfWork.Save();
                }
                catch (DbException ex)
                {
                    Logger.Error("DbException {0}", ex.Message);
                    return View("~/Views/Error/ErrorPage.cshtml",
                        new HandleErrorInfo(ex, ControllerContext.RouteData.Values["controller"].ToString(),
                            ControllerContext.RouteData.Values["action"].ToString()));
                }

                Logger.Info("Service {0} created successfully", service.Name);
                TempData["message"] = "Service created successfully";
                return RedirectToAction("Create", "Services");
            }
            Logger.Info("Service {0} not created", model.Name);
            return RedirectToAction("Create", "Services");
        }

        public PartialViewResult ServiceList()
        {
            Logger.Trace("Starting ServicesController ServiceList method. Previous page {0}", Request.UrlReferrer);
            ViewBag.Message = TempData["message"] as string;
            var model = new ServiceListViewModel {Services = _unitOfWork.Service.GetAll().ToList()};
            return PartialView(model);
        }

        public PartialViewResult ServiceListDelete()
        {
            Logger.Trace("Starting ServicesController ServiceListDelete method. Previous page {0}",
                Request.UrlReferrer);
            ViewBag.Message = TempData["message"] as string;
            var model = new ServiceListViewModel {Services = _unitOfWork.Service.GetAll().ToList()};
            return PartialView(model);
        }

        public PartialViewResult Update(int id)
        {
            Logger.Trace("Launch ServicesController Update method with the parameter id = {0}. Previous page {1}", id,
                Request.UrlReferrer);
            var service = _unitOfWork.Service.Get(id);
            Logger.Trace("Searching Service with Id={0}", id);
            var model = new ServiceViewModel
            {
                Name = service.Name,
                Id = service.Id
            };
            return PartialView(model);
        }

        public ActionResult Delete(int id)
        {
            Logger.Trace("Starting ServicesController Delete method with the parameter id = {0}. Previous page {1}", id,
                Request.UrlReferrer);
            Logger.Trace("Searching Service with Id={0}", id);
            var rates = _unitOfWork.Rate.GetAll().Where(x => x.Service.Id == id);
            foreach (var rate in rates)
            {
                _unitOfWork.Rate.Delete(rate.Id);
                _unitOfWork.File.Delete(rate.Id);
            }

            var service = _unitOfWork.Service.Get(id);
            _unitOfWork.Service.Delete(id);
            try
            {
                _unitOfWork.Save();
            }
            catch (DbException ex)
            {
                Logger.Error("DbException {0}", ex.Message);
                return View("~/Views/Error/ErrorPage.cshtml",
                    new HandleErrorInfo(ex, ControllerContext.RouteData.Values["controller"].ToString(),
                        ControllerContext.RouteData.Values["action"].ToString()));
            }

            Logger.Info("Service {0} deleted", service.Name);
            TempData["message"] = service.Name + "successfully deleted";
            return RedirectToAction("ServiceListDelete", "Services");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ServiceViewModel model)
        {
            Logger.Trace("Starting ServicesController Create method. Previous page {0}", Request.UrlReferrer);
            if (ModelState.IsValid)
            {
                var service = new Service
                {
                    Id = model.Id,
                    Name = model.Name
                };

                try
                {
                    _unitOfWork.Service.Update(service);
                    _unitOfWork.Save();
                }
                catch (DbUpdateException ex)
                {
                    Logger.Error("DbUpdateException {0}", ex.Message);
                    return View("~/Views/Error/ErrorPage.cshtml",
                        new HandleErrorInfo(ex, ControllerContext.RouteData.Values["controller"].ToString(),
                            ControllerContext.RouteData.Values["action"].ToString()));
                }

                Logger.Info("Service {0} succesfully edited", service.Name);
                TempData["message"] = service.Name + "successfully edited";
                return RedirectToAction("ServiceList", "Services");
            }
            Logger.Info("Service {0} not edited", model.Name);
            TempData["message"] = "Element not edited";
            return RedirectToAction("ServiceList", "Services");
        }
    }
}