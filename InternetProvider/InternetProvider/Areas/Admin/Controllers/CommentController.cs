﻿using System.Data.Common;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web.Mvc;
using InternetProvider.BusinessObjects;
using InternetProvider.RepositoryImplementation;
using InternetProvider.ViewModels;
using NLog;

namespace InternetProvider.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin,Moderator")]
    public class CommentController : Controller
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly UnitOfWork _unitOfWork;

        public CommentController(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public ViewResult Index()
        {
            Logger.Trace("Starting CommentController Index method. Previous page {0}", Request.UrlReferrer);
            return View();
        }

        public ViewResult CommentsList()
        {
            Logger.Trace("Starting CommentController CommentsList method. Previous page {0}", Request.UrlReferrer);
            ViewBag.Message = TempData["message"] as string;
            var model = new CommentsListViewModel {Comments = _unitOfWork.Comment.GetAll().ToList()};
            return View(model);
        }

        public ViewResult CommentsListDelete()
        {
            Logger.Trace("Starting CommentController CommentsListDelete method. Previous page {0}",
                Request.UrlReferrer);
            ViewBag.Message = TempData["message"] as string;
            var model = new CommentsListViewModel {Comments = _unitOfWork.Comment.GetAll().ToList()};
            return View(model);
        }

        public PartialViewResult Update(int id)
        {
            Logger.Trace("Starting CommentController Update method with the parameter id = {0}. Previous {1}", id,
                Request.UrlReferrer);
            Logger.Trace("Searching Comment with Id={0}", id);
            var comment = _unitOfWork.Comment.Get(id);
            var model = new CommentViewModel
            {
                AuthorName = comment.AuthorName,
                Time = comment.Time,
                UserComm = comment.UserComm,
                Id = comment.Id,
                NewsId = comment.NewsId
            };
            return PartialView(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CommentViewModel model)
        {
            Logger.Trace("Starting CommentController Create method. Previous page {0}", Request.UrlReferrer);
            if (ModelState.IsValid)
            {
                var comment = new Comment
                {
                    Id = model.Id,
                    AuthorName = model.AuthorName,
                    Time = model.Time,
                    UserComm = model.UserComm,
                    NewsId = model.NewsId
                };

                try
                {
                    _unitOfWork.Comment.Update(comment);
                    _unitOfWork.Save();
                }
                catch (DbUpdateException ex)
                {
                    Logger.Error("DbUpdateException {0}", ex.Message);
                    return View("~/Views/Error/ErrorPage.cshtml",
                        new HandleErrorInfo(ex, ControllerContext.RouteData.Values["controller"].ToString(),
                            ControllerContext.RouteData.Values["action"].ToString()));
                }

                Logger.Info("Comment Editing with id={0}", model.Id);
                TempData["message"] = "Comment successfully edited";
                return RedirectToAction("CommentsList", "Comment");
            }
            Logger.Info("Comment Id={0} not edited", model.Id);
            TempData["message"] = "Element not edited";
            return RedirectToAction("CommentsListDelete", "Comment");
        }

        public ActionResult Delete(int id)
        {
            Logger.Trace("Starting CommentController Delete method with the parameter id = {0}. Previous page{1}", id,
                Request.UrlReferrer);
            try
            {
                _unitOfWork.Comment.Delete(id);
                _unitOfWork.Save();
            }
            catch (DbException ex)
            {
                Logger.Error("DbException {0}", ex.Message);
                return View("~/Views/Error/ErrorPage.cshtml",
                    new HandleErrorInfo(ex, ControllerContext.RouteData.Values["controller"].ToString(),
                        ControllerContext.RouteData.Values["action"].ToString()));
            }

            Logger.Info("Comment with Id={0} successfully deleted", id);
            TempData["message"] = "Comment successfully deleted";
            return RedirectToAction("CommentsListDelete", "Comment");
        }
    }
}