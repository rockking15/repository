﻿using System.Web.Mvc;
using InternetProvider.Filters;

namespace InternetProvider
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            //  filters.Add(new HandleErrorAttribute());
            filters.Add(new HandleAllErrorAttribute {View = "~/Views/Error/ErrorPage.cshtml"});
        }
    }
}