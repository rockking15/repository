﻿using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using InternetProvider.Filters;
using InternetProvider.RepositoryImplementation;
using InternetProvider.ViewModels;
using NLog;
using WebMatrix.WebData;

namespace InternetProvider.Controllers
{
    [Authorize]
    [InitializeSimpleMembership]
    public class LogInController : Controller
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly UnitOfWork _unitOfWork;

        public LogInController(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [AllowAnonymous]
        public ViewResult Index()
        {
            Logger.Trace("Starting LogInController Index method. Previous page: {0}", Request.UrlReferrer);
            var model = new LogInViewModel();
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Index(LogInViewModel model)
        {
            Logger.Trace("Starting LogInController Index method. Previous page: {0}", Request.UrlReferrer);
            Logger.Trace("Authorization and verification of the data entered");
            if (ModelState.IsValid && WebSecurity.Login(model.Email, model.Password, model.RememberMe))
            {
                var rolesForUser = Roles.GetRolesForUser(model.Email).FirstOrDefault();
                var account = _unitOfWork.Account.GetAll().First(x => x.User.Email == model.Email);
                if ((rolesForUser != null) &&
                    (rolesForUser.Contains("User") || rolesForUser.Contains("Admin") ||
                     rolesForUser.Contains("Moderator")) && (account.Debt == 0))
                {
                    Logger.Info("User {0} successfully logged on", account.User.Email);
                    return RedirectToAction("Index", "Home");
                }

                ViewBag.Message = "Your account has been blocked";
                if (account.Debt < 0)
                {
                    if ((rolesForUser != null) && (account.Debt + account.Money >= 0) &&
                        (rolesForUser.Contains("User") || rolesForUser.Contains("Admin") ||
                         rolesForUser.Contains("Moderator")))
                    {
                        account.Money += account.Debt;
                        account.Debt = 0;
                        _unitOfWork.Save();
                        Logger.Info("User {0} to repay debt and successfully logged in", account);
                        return RedirectToAction("Index", "Home");
                    }
                    Logger.Info("User {0} has been denied access. Debt: {1}", account, account.Debt);
                    ViewBag.Message = "Your account is blocked! Your Debt: " + account.Debt;
                }
                WebSecurity.Logout();
                return View(model);
            }
            Logger.Info("User {0} has entered an incorrect username or password", model.Email);
            ViewBag.Message = "Invalid Email or Password";
            return View(model);
        }

        public ActionResult LogOff()
        {
            var user = _unitOfWork.User.Find(User.Identity.Name);
            Logger.Info("User {0} logged out", user.Email);
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "LogIn");
        }
    }
}