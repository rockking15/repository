﻿using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web.Mvc;
using InternetProvider.RepositoryImplementation;
using InternetProvider.ViewModels;
using NLog;

namespace InternetProvider.Controllers
{
    [AllowAnonymous]
    public class AdditionalServicesController : Controller
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly UnitOfWork _unitOfWork;

        public AdditionalServicesController(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public ViewResult Index()
        {
            Logger.Trace("Starting AdditionalServicesController  Index method. Previous page {0}",
                Request.UrlReferrer);
            var model = new AdditionalFeatureListViewModel
            {
                AdditionalFeatures = _unitOfWork.AdditionalFeature.GetAll().ToList()
            };
            return View(model);
        }

        public ActionResult Order(int id)
        {
            Logger.Trace(
                "Starting AdditionalServicesController  Order method with parameter id={0}. Previous page {1}", id,
                Request.UrlReferrer);

            Logger.Info("Searching additionalService with Id={0}", id);
            var additionalFeature = _unitOfWork.AdditionalFeature.Get(id);

            Logger.Trace("Checking user authentication");
            if (User.Identity.IsAuthenticated)
            {
                var user = _unitOfWork.User.Find(User.Identity.Name);
                try
                {
                    user.Account.AdditionalFeatures.Add(additionalFeature);
                    _unitOfWork.Save();
                    Logger.Info("User {0} successfully bought additionalService {1}", User.Identity.Name,
                        additionalFeature.Name);
                }
                catch (DbUpdateException ex)
                {
                    Logger.Error("User {0} Added Additional Features {1} with Exception {2}", user.Email,
                        additionalFeature.Name, ex.Message);
                    ModelState.AddModelError("User", ex.Message);
                    return View("~/Views/Error/ErrorPage.cshtml",
                        new HandleErrorInfo(ex, ControllerContext.RouteData.Values["controller"].ToString(),
                            ControllerContext.RouteData.Values["action"].ToString()));
                }
                return Content("Additional service is successfully added to the Personal Cabinet");
            }
            Logger.Info("User tried to order additional service, but he was not logged in the system");
            return Content("Please, login to your personal account or register");
        }
    }
}