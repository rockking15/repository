﻿using System.Web.Mvc;
using NLog;

namespace InternetProvider.Controllers
{
    public class TermsController : Controller
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public ViewResult Index()
        {
            Logger.Trace("Starting TermsController Index method. Previous page {0}", Request.UrlReferrer);
            return View();
        }
    }
}