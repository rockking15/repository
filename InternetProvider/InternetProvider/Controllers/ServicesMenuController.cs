﻿using System.Linq;
using System.Web.Mvc;
using InternetProvider.RepositoryImplementation;
using InternetProvider.ViewModels;
using NLog;

namespace InternetProvider.Controllers
{
    public class ServicesMenuController : Controller
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly UnitOfWork _unitOfWork;

        public ServicesMenuController(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public PartialViewResult Index()
        {
            Logger.Trace("Starting ServicesMenuController Index method. Previous page {0}", Request.UrlReferrer);
            var model = new ServiceListViewModel {Services = _unitOfWork.Service.GetAll().ToList()};
            return PartialView(model);
        }
    }
}