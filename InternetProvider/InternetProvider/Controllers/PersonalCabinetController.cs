﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web.Mvc;
using InternetProvider.BusinessObjects;
using InternetProvider.RepositoryImplementation;
using InternetProvider.ViewModels;
using NLog;

namespace InternetProvider.Controllers
{
    [Authorize(Roles = "User,Admin,Moderator")]
    public class PersonalCabinetController : Controller
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly UnitOfWork _unitOfWork;

        public PersonalCabinetController(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public ViewResult Index()
        {
            Logger.Trace("Starting PersonalCabinet Controller Index method. Previous page {0}", Request.UrlReferrer);
            var user = _unitOfWork.User.Find(User.Identity.Name);

            var model = new PersonalCabinetViewModel
            {
                Adress = user.Adress,
                DateOfBirthday = user.DateOfBirthday,
                Email = user.Email,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Phone = user.Phone,
                Account = user.Account
            };
            model.Account.Rates = _unitOfWork.Rate.GetRates(user.Account.Id).ToList();
            model.Account.AdditionalFeatures =
                _unitOfWork.AdditionalFeature.GetAdditionalFeatures(user.Account.Id).ToList();
            model.ApplicationTime = DateTime.Now;
            return View(model);
        }

        public ViewResult Edit()
        {
            Logger.Trace("Starting PersonalCabinet Controller Edit method. Previous page {0}", Request.UrlReferrer);
            ViewBag.Message = TempData["message"] as string;
            var user = _unitOfWork.User.Find(User.Identity.Name);
            var model = new UpdateUserViewModel
            {
                Id = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Adress = user.Adress,
                Phone = user.Phone,
                Email = user.Email,
                DateOfBirthday = user.DateOfBirthday
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveNewData(UpdateUserViewModel model)
        {
            Logger.Trace("Starting PersonalCabinetController SaveNewData method. Previous page {0}",
                Request.UrlReferrer);
            if (ModelState.IsValid)
            {
                var user = new User
                {
                    Id = model.Id,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    Adress = model.Adress,
                    DateOfBirthday = model.DateOfBirthday,
                    Email = model.Email,
                    Phone = model.Phone
                };
                try
                {
                    _unitOfWork.User.Update(user);
                    _unitOfWork.Save();
                }
                catch (DbUpdateException ex)
                {
                    Logger.Error("DbUpdateException {0}", ex.Message);
                    return View("~/Views/Error/ErrorPage.cshtml",
                        new HandleErrorInfo(ex, ControllerContext.RouteData.Values["controller"].ToString(),
                            ControllerContext.RouteData.Values["action"].ToString()));
                }

                TempData["message"] = "The user data have been updated";
                Logger.Info("The user data have been updated {0}", model.Email);
                return RedirectToAction("Edit");
            }
            Logger.Info("User data {0} were not updated", model.Email);
            return View("Edit", model);
        }

        public ActionResult Send(int id)
        {
            Logger.Trace("Starting PersonalCabinetController Send method with parameter id={0}. Previous page {1}", id,
                Request.UrlReferrer);
            Logger.Trace("Searching Rate with Id={0}", id);
            var rate = _unitOfWork.Rate.Get(id);
            var user = _unitOfWork.User.Find(User.Identity.Name);

            if (user.Account.Money < rate.Price)
                return Content("Not enough money in the account to pay for this rate");

            if (ModelState.IsValid)
            {
                var application = new Application
                {
                    FirstName = user.FirstName,
                    Adress = user.Adress,
                    LastName = user.LastName,
                    Phone = user.Phone,
                    ApplicationTime = DateTime.Now,
                    Account = user.Account,
                    RateId = rate.Id,
                    Status = "New"
                };
                try
                {
                    _unitOfWork.Application.Create(application);
                    _unitOfWork.Save();
                }
                catch (DbUpdateException ex)
                {
                    Logger.Error("DbUpdateException {0}", ex.Message);
                    return View("~/Views/Error/ErrorPage.cshtml",
                        new HandleErrorInfo(ex, ControllerContext.RouteData.Values["controller"].ToString(),
                            ControllerContext.RouteData.Values["action"].ToString()));
                }

                Logger.Info("User {0} successfully created an application {1}.", user.Email, application.Id);
                return Content("Application successfully created");
            }
            Logger.Info("Application User {0} has not been created.", user.Email);
            return View("Index");
        }

        public ActionResult ApplicationAdditionalFeatures(int id)
        {
            Logger.Trace(
                "Starting PersonalCabinetController ApplicationAdditionalFeatures method with parameter id={0}. Previous page {1}",
                id, Request.UrlReferrer);
            Logger.Trace("Searching AdditionalFeature with Id={0}", id);
            var additionalfeature = _unitOfWork.AdditionalFeature.Get(id);
            var user = _unitOfWork.User.Find(User.Identity.Name);

            if (user.Account.Money < additionalfeature.Price)
                return Content("Not enough money in the account to pay for this Additionalfeature");

            if (ModelState.IsValid)
            {
                var application = new Application
                {
                    FirstName = user.FirstName,
                    Adress = user.Adress,
                    LastName = user.LastName,
                    Phone = user.Phone,
                    ApplicationTime = DateTime.Now,
                    Account = user.Account,
                    AdditionalFeatures = new List<AdditionalFeature> {additionalfeature},
                    Status = "New"
                };

                try
                {
                    _unitOfWork.Application.Create(application);
                    _unitOfWork.Save();
                }
                catch (DbUpdateException ex)
                {
                    Logger.Error("DbUpdateException {0}", ex.Message);
                    return View("~/Views/Error/ErrorPage.cshtml",
                        new HandleErrorInfo(ex, ControllerContext.RouteData.Values["controller"].ToString(),
                            ControllerContext.RouteData.Values["action"].ToString()));
                }

                Logger.Info("User {0} successfully created an application {1}.", user.Email, application.Id);
                return Content("Application successfully created");
            }
            Logger.Info("Application User {0} has not been created.", user.Email);
            return View("Index");
        }

        public ActionResult DeleteRate(int id)
        {
            Logger.Trace(
                "Run PersonalCabinet Controller DeleteRate method with parameter id = {0}. Previous page : {1}", id,
                Request.UrlReferrer);
            var user = _unitOfWork.User.Find(User.Identity.Name);
            Logger.Trace("Searching rate with id = {0}", id);
            foreach (var item in user.Account.Rates.ToList().Where(a => a.Id == id))
            {
                try
                {
                    user.Account.Rates.Remove(item);
                    _unitOfWork.Save();
                }
                catch (DbUpdateException ex)
                {
                    Logger.Error("DbUpdateException {0}", ex.Message);
                    return View("~/Views/Error/ErrorPage.cshtml",
                        new HandleErrorInfo(ex, ControllerContext.RouteData.Values["controller"].ToString(),
                            ControllerContext.RouteData.Values["action"].ToString()));
                }

                Logger.Info("User {0} deleted {1} rate.", user.Email, item.Name);
            }


            return RedirectToAction("Index");
        }

        public ActionResult DeleteAdditional(int id)
        {
            Logger.Trace(
                "Run PersonalCabinet Controller DeleteAdditional method with parameter id = {0}. Previous page : {1}",
                id,
                Request.UrlReferrer);
            var user = _unitOfWork.User.Find(User.Identity.Name);
            Logger.Trace("Searching AdditionalFeature in account with id = {0}", id);
            foreach (var item in user.Account.AdditionalFeatures.ToList().Where(a => a.Id == id))
            {
                try
                {
                    user.Account.AdditionalFeatures.Remove(item);
                    _unitOfWork.Save();
                }
                catch (DbUpdateException ex)
                {
                    Logger.Error("DbUpdateException {0}", ex.Message);
                    return View("~/Views/Error/ErrorPage.cshtml",
                        new HandleErrorInfo(ex, ControllerContext.RouteData.Values["controller"].ToString(),
                            ControllerContext.RouteData.Values["action"].ToString()));
                }

                Logger.Info("User {0} removed an additional service {1}.", user.Email, item.Name);
            }
            return RedirectToAction("Index");
        }
    }
}