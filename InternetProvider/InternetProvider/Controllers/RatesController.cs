﻿using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web.Mvc;
using InternetProvider.RepositoryImplementation;
using InternetProvider.ViewModels;
using NLog;

namespace InternetProvider.Controllers
{
    [AllowAnonymous]
    public class RatesController : Controller
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly UnitOfWork _unitOfWork;

        public RatesController(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public ViewResult Index(int id)
        {
            Logger.Trace("Starting RateController Index method with parameter id = {0}. Previous page {1}", id,
                Request.UrlReferrer);
            var service = _unitOfWork.Service.Get(id);
            Logger.Trace("Searching Service with parameter id={0} and get all its rates", id);
            var model = new RateListViewModel
            {
                Rates = service.Rates.ToList(),
                ServiceId = service.Rates.Select(x => x.Service.Id).First(),
                ServiceName = service.Name
            };
            return View(model);
        }

        public ActionResult Details(int id)
        {
            Logger.Trace("Starting RateController Details method with the parameter id = {0}. Previous page {1}", id,
                Request.UrlReferrer);
            if (User.Identity.IsAuthenticated)
            {
                var user = _unitOfWork.User.Find(User.Identity.Name);

                var rate = _unitOfWork.Rate.Get(id);

                if (user.Account.Rates.Contains(_unitOfWork.Rate.Get(id)))
                {
                    Logger.Info("User {0} attempted to re-connect rate {1}", user.Email, rate.Name);
                    return Content("You have already connected  to this rate");
                }
                if (user.Account.Rates.Any(item => item.ServiceId == rate.ServiceId))
                {
                    Logger.Info(
                        "User {0} attempted to connect rate {1}, but he is already connected to One of the rates of the service ServiceId={2}",
                        user.Email, rate.Name, rate.ServiceId);
                    return Content("You have already connected to one of the rates for this service");
                }
                user.Account.Rates.Add(_unitOfWork.Rate.Get(id));
                try
                {
                    _unitOfWork.Save();
                }
                catch (DbUpdateException ex)
                {
                    Logger.Error("DbUpdateException {0}", ex.Message);
                    return View("~/Views/Error/ErrorPage.cshtml",
                        new HandleErrorInfo(ex, ControllerContext.RouteData.Values["controller"].ToString(),
                            ControllerContext.RouteData.Values["action"].ToString()));
                }

                Logger.Info("User {0} successfully connect rate {1}", user.Email, rate.Name);
                return Content("The data has been successfully added to the personal cabinet");
            }
            Logger.Info("The user tried to connect the rate, but it was not logged in the system");
            return Content("Please, login to your personal account or register");
        }

        public ViewResult SortByPriceDes(int id)
        {
            Logger.Trace("Starting RateController SortByPriceDes method with parameter id = {0}. Previous page {1}",
                id, Request.UrlReferrer);
            var rates = _unitOfWork.Rate.SortByPriceDes(id).ToList();
            var service = _unitOfWork.Service.Get(id);
            var model = new RateListViewModel
            {
                Rates = rates,
                ServiceId = id,
                ServiceName = service.Name
            };
            return View("Index", model);
        }

        public ViewResult SortByPriceInc(int id)
        {
            Logger.Trace("Starting RateController SortByPriceInc method with parameter id = {0}. Previous page {1}",
                id, Request.UrlReferrer);
            var rates = _unitOfWork.Rate.SortByPriceInc(id).ToList();
            var service = _unitOfWork.Service.Get(id);
            var model = new RateListViewModel
            {
                Rates = rates,
                ServiceId = id,
                ServiceName = service.Name
            };
            return View("Index", model);
        }

        public ViewResult SortByAlph(int id)
        {
            Logger.Trace("Starting RateController SortByAlph method with parameter id = {0}. Previous page {1}",
                id, Request.UrlReferrer);
            var rates = _unitOfWork.Rate.SortByAlph(id).ToList();
            var service = _unitOfWork.Service.Get(id);
            var model = new RateListViewModel
            {
                Rates = rates,
                ServiceId = id,
                ServiceName = service.Name
            };
            return View("Index", model);
        }

        public ViewResult SortByAlphDec(int id)
        {
            Logger.Trace("Starting RateController SortByAlphDec method with parameter id = {0}. Previous page {1}",
                id, Request.UrlReferrer);
            var rates = _unitOfWork.Rate.SortByAlphDec(id).ToList();
            var service = _unitOfWork.Service.Get(id);
            var model = new RateListViewModel
            {
                Rates = rates,
                ServiceId = id,
                ServiceName = service.Name
            };
            return View("Index", model);
        }

        public FileResult DownloadPdf(int id)
        {
            Logger.Trace("Starting RateController DownloadPdf method with parameter id = {0}. Previous {1}",
                id, Request.UrlReferrer);
            Logger.Trace("There is a file download with id = {0}", id);
            var rate = _unitOfWork.Rate.Get(id);
            var path = "~/Content/RatesFiles/" + rate.File.Name;
            return File(path, "application/pdf", rate.File.Name);
        }
    }
}