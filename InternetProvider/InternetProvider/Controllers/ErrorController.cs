﻿using System.Web.Mvc;
using InternetProvider.RepositoryImplementation;
using NLog;

namespace InternetProvider.Controllers
{
    [AllowAnonymous]
    public class ErrorController : Controller
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly UnitOfWork _unitOfWork;

        public ErrorController(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public ViewResult NotFound()
        {
            Logger.Info("Starting ErrorController NotFound method. Page not found : {0}", Request.UrlReferrer);
            return View();
        }

        public ViewResult ErrorPage()
        {
            Logger.Info("Starting ErrorController ErrorPage method. Error on the page: {0}", Request.UrlReferrer);
            return View();
        }
    }
}