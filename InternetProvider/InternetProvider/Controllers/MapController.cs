﻿using System.Web.Mvc;
using InternetProvider.RepositoryImplementation;
using NLog;

namespace InternetProvider.Controllers
{
    [AllowAnonymous]
    public class MapController : Controller
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly UnitOfWork _unitOfWork;

        public MapController(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public ViewResult Index()
        {
            Logger.Trace("Starting MapController Index method. Previous page {0}", Request.UrlReferrer);
            return View();
        }

        public JsonResult GetData()
        {
            var markers = _unitOfWork.Map.GetAll();
            return Json(markers, JsonRequestBehavior.AllowGet);
        }
    }
}