﻿using System.Text;
using System.Threading;
using System.Web.Mvc;
using InternetProvider.BusinessLogic;
using InternetProvider.RepositoryImplementation;
using InternetProvider.ViewModels;
using NLog;

namespace InternetProvider.Controllers
{
    [AllowAnonymous]
    public class ContactInformationController : Controller
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly UnitOfWork _unitOfWork;

        public ContactInformationController(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public ViewResult Index()
        {
            Logger.Trace("Starting ContactInformationController Index method. Previous page : {0}",
                Request.UrlReferrer);
            ViewBag.Message = TempData["message"] as string;
            var model = new ContactPageViewModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Contact(ContactPageViewModel model)
        {
            Logger.Trace("Starting ContactInformationController Contact method. Previous page : {0}",
                Request.UrlReferrer);
            var contactService = new ContactService();
            if (ModelState.IsValid)
            {
                const string toAddress = "rockking151@gmail.com";
                var fromAddress = model.Email;
                var subject = model.Subject;
                var message = new StringBuilder();
                message.Append("Name: " + model.Name + "\n");
                message.Append("Email: " + model.Email + "\n");
                message.Append("Subject: " + model.Subject + "\n\n");
                message.Append(model.Message);

                var tEmail = new Thread(() =>
                        contactService.SendEmail(toAddress, fromAddress, subject, message));
                tEmail.Start();

                Logger.Info("Letter from the contact form has been sent by the user {0}", model.Email);
                TempData["message"] = "Message successfully sent";
                return RedirectToAction("Index");
            }
            TempData["message"] = "The letter was not sent";
            Logger.Info("The letter from user {0}  was not sent", model.Email);
            return RedirectToAction("Index");
        }
    }
}