﻿using System.Linq;
using System.Web.Mvc;
using InternetProvider.BusinessLogic;
using InternetProvider.RepositoryImplementation;
using InternetProvider.ViewModels;
using NLog;

namespace InternetProvider.Controllers
{
    [AllowAnonymous]
    public class HomeController : Controller
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly UnitOfWork _unitOfWork;

        public HomeController(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public ViewResult Index()
        {
            Logger.Trace("Starting HomeController Index method. Previous page : {0}", Request.UrlReferrer);
            var newsService = new NewsService();
            var advantageService = new AdvantageService();
            Logger.Trace("Creating services {0} , {1}", newsService, advantageService);
            var model = new HomeViewModel
            {
                Newses = newsService.CutText(_unitOfWork.News.OrderByDescendingPublicationDate().ToList()),
                Advantages = AdvantageService.CutText(_unitOfWork.Advantage.GetAll().ToList())
            };
            return View(model);
        }
    }
}