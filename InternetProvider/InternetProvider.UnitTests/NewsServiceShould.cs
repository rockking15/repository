﻿using System;
using System.Collections.Generic;
using FluentAssertions;
using InternetProvider.BusinessLogic;
using InternetProvider.BusinessObjects;
using NUnit.Framework;

namespace InternetProvider.UnitTests
{
    [TestFixture]
    public class NewsServiceShould
    {
        public void CreatePictureAndCheckExtensions()
        {
            NewsService.CreatePicture(null).Should().BeOfType(typeof(string));

            NewsService.Checkextension(null).Should().BeGreaterOrEqualTo(0);
        }

        public void CutTextPreview()
        {
            var list = new List<News>
            {
                new News
                {
                    Id = 3,
                    Author = "Moderator",
                    PublicationDate = DateTime.Now.AddDays(-25),
                    Text =
                        "HOLIDAYS. Holidays are 'holy days,' when people interrupt the profane, mundane round of production and celebrate with " +
                        "the preparation and eating of special foods and meals. The two basic forms of holidays are a festival (from Latin festum " +
                        "for 'feast'), when people break their normal weekly, monthly, or annual routine to celebrate together, and a vacation (in the " +
                        "sense of leaving their homes and workplaces empty), when an often longer disruption may be accompanied by dislocation, as people " +
                        "change residences or travel.",
                    Title = "Holidays and Shares"
                }
            };


            NewsService.CutTextPreview(list).Should().NotBeEmpty();

            NewsService.CutTextPreview(list).Should().NotContain(x => x.Text.Length > 50);
        }
    }
}