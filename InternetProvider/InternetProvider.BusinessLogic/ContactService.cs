﻿using System;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Web;

namespace InternetProvider.BusinessLogic
{
    public class ContactService
    {
        public void SendEmail(string toAddress, string fromAddress,
            string subject, StringBuilder message)
        {
            try
            {
                using (var mail = new MailMessage())
                {
                    const string email = "rockking151@gmail.com";
                    const string password = "qAz123123";

                    var loginInfo = new NetworkCredential(email, password);

                    mail.From = new MailAddress(fromAddress);
                    mail.To.Add(new MailAddress(toAddress));
                    mail.Subject = subject;
                    mail.Body = message.ToString();
                    mail.IsBodyHtml = true;

                    try
                    {
                        using (var smtpClient = new SmtpClient(
                            "smtp.gmail.com", 587))
                        {
                            smtpClient.EnableSsl = true;
                            smtpClient.UseDefaultCredentials = false;
                            smtpClient.Credentials = loginInfo;
                            smtpClient.Send(mail);
                        }
                    }

                    finally
                    {
                        mail.Dispose();
                    }
                }
            }
            catch (SmtpFailedRecipientsException ex)
            {
                foreach (var t in ex.InnerExceptions)
                {
                    var status = t.StatusCode;
                    if ((status == SmtpStatusCode.MailboxBusy) ||
                        (status == SmtpStatusCode.MailboxUnavailable))
                    {
                        HttpContext.Current.Response.Write("Delivery failed - retrying in 5 seconds.");
                        Thread.Sleep(5000);
                    }
                    else
                    {
                        HttpContext.Current.Response.Write("Failed to deliver message");
                    }
                }
            }
            catch (SmtpException se)
            {
                HttpContext.Current.Response.Write(se.ToString());
            }

            catch (Exception ex)
            {
                HttpContext.Current.Response.Write(ex.ToString());
            }
        }
    }
}