﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using ImageResizer;
using InternetProvider.BusinessObjects;

namespace InternetProvider.BusinessLogic
{
    public class NewsService
    {
        public static int Checkextension(HttpPostedFileBase upload)
        {
            var counter = 0;
            if ((upload != null) && (upload.ContentLength > 0))
            {
                var allowedExtensions = new[] {".jpg", ".jpeg", ".png"};
                var extension = Path.GetExtension(upload.FileName);
                if (extension != null)
                {
                    var checkextension = extension.ToLower();
                    counter += allowedExtensions.Count(itm => itm.Contains(checkextension));
                }
            }
            return counter;
        }

        public static List<News> CutTextPreview(List<News> newsList)
        {
            foreach (var news in newsList.Where(news => news.Text.Length > 50))
                news.Text = news.Text.Substring(0, 50);
            return newsList;
        }

        public static string CreatePicture(HttpPostedFileBase upload)
        {
            const string servpath = "~/Images/";
            var path = HttpContext.Current.Server.MapPath(servpath);
            upload.InputStream.Seek(0, SeekOrigin.Begin);

            ImageBuilder.Current.Build(
                new ImageJob(
                    upload.InputStream,
                    path + upload.FileName,
                    new Instructions("maxwidth=352&maxheight=230"),
                    false,
                    false));
            return servpath;
        }

        public List<News> CutText(List<News> newsList)
        {
            foreach (var news in newsList.Where(news => news.Text.Length > 200))
                news.Text = news.Text.Substring(0, 200);
            return newsList;
        }
    }
}