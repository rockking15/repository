﻿using System.Text.RegularExpressions;
using System.Web.Security;

namespace InternetProvider.BusinessLogic
{
    public class UserService
    {
        public static string GetPassword()
        {
            var password = Membership.GeneratePassword(8, 0);
            var newPassword = Regex.Replace(password, @"[^a-zA-Z0-9]", m => "9");
            return newPassword;
        }
    }
}