﻿using System.IO;
using System.Web;

namespace InternetProvider.BusinessLogic
{
    public class RateService
    {
        public bool UploadFile(HttpPostedFileBase uploadFile)
        {
            var ext = false;
            var directory = HttpContext.Current.Server.MapPath("~/Content/RatesFiles");
            if ((uploadFile != null) && (uploadFile.ContentLength > 0))
            {
                var fileName = Path.GetFileName(uploadFile.FileName);
                if (fileName != null) uploadFile.SaveAs(Path.Combine(directory, fileName));
                var allowedExtensions = new[] {".pdf"};
                var extension = Path.GetExtension(uploadFile.FileName);
                if (extension != null)
                {
                    var checkextension = extension.ToLower();
                    foreach (var itm in allowedExtensions)
                        if (itm.Contains(checkextension))
                            ext = true;
                }
            }
            return ext;
        }
    }
}