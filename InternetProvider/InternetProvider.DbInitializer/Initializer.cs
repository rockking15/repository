﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading;
using System.Web.Security;
using InternetProvider.BusinessObjects;
using InternetProvider.DbContext;
using WebMatrix.WebData;

namespace InternetProvider.DbInitializer
{
    public class Initializer : DropCreateDatabaseIfModelChanges<InternetProviderContext>
    {
        private static SimpleMembershipInitializer _initializer;
        private static object _initializerLock = new object();
        private static bool _isInitialized;

        protected override void Seed(InternetProviderContext context)
        {
            LazyInitializer.EnsureInitialized(ref _initializer, ref _isInitialized, ref _initializerLock);
            var roles = (SimpleRoleProvider) Roles.Provider;
            var membership = (SimpleMembershipProvider) Membership.Provider;

            if (!roles.RoleExists("Admin"))
                roles.CreateRole("Admin");

            if (!roles.RoleExists("User"))
                roles.CreateRole("User");

            if (!roles.RoleExists("Moderator"))
                roles.CreateRole("Moderator");

            if (membership.GetUser("admin", false) == null)

            {
                WebSecurity.CreateUserAndAccount("admin@mail.ru", "123123",
                    new
                    {
                        FirstName = "admin",
                        LastName = "admin",
                        DateOfBirthday = "1990-01-05",
                        Adress = "Ukraine",
                        Phone = "2281488",
                        RegistrationTime = DateTime.Now.AddYears(-5)
                    });

                roles.AddUsersToRoles(new[] {"admin@mail.ru"}, new[] {"Admin"});
            }

            if (membership.GetUser("moderator", false) == null)

            {
                WebSecurity.CreateUserAndAccount("moderator@gmail.com", "123123",
                    new
                    {
                        FirstName = "moder",
                        LastName = "moder",
                        DateOfBirthday = "2000-03-05",
                        Adress = "Kharkiv",
                        Phone = "125521",
                        RegistrationTime = DateTime.Now.AddYears(-3)
                    });

                roles.AddUsersToRoles(new[] {"moderator@gmail.com"}, new[] {"Moderator"});
            }

            var addfeature1 = new AdditionalFeature
            {
                Id = 1,
                Name = "Setting WI Fi Router",
                Price = 50
            };
            var addfeature2 = new AdditionalFeature {Id = 2, Name = "Dynamic IP", Price = 30};
            var addfeature3 = new AdditionalFeature
            {
                Id = 3,
                Name = "Setting IPTV player on the customer's PC",
                Price = 30
            };
            var addfeature4 = new AdditionalFeature
            {
                Id = 4,
                Name = "Setting SMART TV for IPTV",
                Price = 25
            };
            var addfeature5 = new AdditionalFeature {Id = 5, Name = "Laptop Diagnostics", Price = 200};
            var addfeature6 = new AdditionalFeature {Id = 6, Name = "Antivirus", Price = 50};
            var addfeature7 = new AdditionalFeature {Id = 7, Name = "Connector", Price = 120};
            var addfeature8 = new AdditionalFeature {Id = 8, Name = "Online outlet", Price = 25};
            var addfeature9 = new AdditionalFeature {Id = 9, Name = "Setting up TV channels", Price = 40};
            var addfeature10 = new AdditionalFeature {Id = 10, Name = "Software Installation", Price = 100};

            var addf = new List<AdditionalFeature>
            {
                addfeature1,
                addfeature2,
                addfeature3,
                addfeature4,
                addfeature5,
                addfeature6,
                addfeature7,
                addfeature8,
                addfeature9,
                addfeature10
            };
            addf.ForEach(d => context.AdditionalFeatures.Add(d));
            context.SaveChanges();

            var serv1 = new Service
            {
                Id = 1,
                Name = "Internet"
            };
            var serv2 = new Service
            {
                Id = 2,
                Name = "Phone"
            };
            var serv3 = new Service {Id = 3, Name = "KTV"};
            var serv4 = new Service {Id = 4, Name = "IPTV"};
            var services = new List<Service>
            {
                serv1,
                serv2,
                serv3,
                serv4
            };

            services.ForEach(d => context.Services.Add(d));
            context.SaveChanges();

            var rate1 = new Rate
            {
                Id = 1,
                Name = "Social",
                Price = 70,
                Description = "1GB/s",
                File = new File {Id = 1, Name = "Social.pdf"},
                ServiceId = 1
            };
            var rate2 = new Rate
            {
                Id = 2,
                Name = "Standart10",
                Price = 100,
                Description = "10GB/s",
                File = new File {Id = 2, Name = "Standart10.pdf"},
                ServiceId = 1
            };
            var rate3 = new Rate
            {
                Id = 3,
                Name = "Standart30",
                Price = 125,
                Description = "30GB/s",
                File = new File {Id = 3, Name = "Standart30.pdf"},
                ServiceId = 1
            };
            var rate4 = new Rate
            {
                Id = 4,
                Name = "Middle",
                Price = 150,
                Description = "50GB/s",
                File = new File {Id = 4, Name = "Middle.pdf"},
                ServiceId = 1
            };
            var rate5 = new Rate
            {
                Id = 5,
                Name = "Full",
                Price = 200,
                Description = "100GB/s",
                File = new File {Id = 5, Name = "Full.pdf"},
                ServiceId = 1
            };
            var rate6 = new Rate
            {
                Id = 6,
                Name = "Vip",
                Price = 500,
                Description = "Calls to city",
                File = new File {Id = 6, Name = "Vip.pdf"},
                ServiceId = 2
            };
            var rate7 = new Rate
            {
                Id = 7,
                Name = "Standart",
                Price = 200,
                Description = "Calls abroad",
                File = new File {Id = 7, Name = "Standart.pdf"},
                ServiceId = 2
            };
            var rate8 = new Rate
            {
                Id = 8,
                Name = "Free",
                Price = 2000,
                Description = "abroad+city",
                File = new File {Id = 8, Name = "Free.pdf"},
                ServiceId = 2
            };
            var rate9 = new Rate
            {
                Id = 9,
                Name = "LuxCall",
                Price = 580,
                Description = "free calls to mobile phones",
                File = new File {Id = 9, Name = "LuxCall.pdf"},
                ServiceId = 2
            };

            var rate10 = new Rate
            {
                Id = 10,
                Name = "Unlimited",
                Price = 650,
                Description = "mobile+abroad",
                File = new File {Id = 10, Name = "Unlimited.pdf"},
                ServiceId = 2
            };

            var rate11 = new Rate
            {
                Id = 11,
                Name = "Base",
                Price = 20,
                Description = "IPTV 10Mb/s",
                File = new File {Id = 11, Name = "Base.pdf"},
                ServiceId = 3
            };
            var rate12 = new Rate
            {
                Id = 12,
                Name = "Base+",
                Price = 85,
                Description = "IPTV 100Mb/s - 100 Programs",
                File = new File {Id = 12, Name = "Base+.pdf"},
                ServiceId = 3
            };
            var rate13 = new Rate
            {
                Id = 13,
                Name = "Luxury",
                Price = 200,
                Description = "Luxury Package",
                File = new File {Id = 13, Name = "Luxury.pdf"},
                ServiceId = 3
            };
            var rate14 = new Rate
            {
                Id = 14,
                Name = "KTV+",
                Price = 155,
                Description = "high quality",
                File = new File {Id = 14, Name = "KTV+.pdf"},
                ServiceId = 4
            };
            var rate15 = new Rate
            {
                Id = 15,
                Name = "KTV-",
                Price = 169,
                Description = "new technologies",
                File = new File {Id = 15, Name = "KTV-.pdf"},
                ServiceId = 4
            };
            var rates = new List<Rate>
            {
                rate1,
                rate2,
                rate3,
                rate4,
                rate5,
                rate6,
                rate7,
                rate8,
                rate9,
                rate10,
                rate11,
                rate12,
                rate13,
                rate14,
                rate15
            };

            rates.ForEach(d => context.Rates.Add(d));
            context.SaveChanges();

            var acc = new Account {Id = 1, Money = 200, Rates = new List<Rate> {rate1}, UserId = 1};
            var acc1 = new Account {Id = 2, Money = 0, Rates = new List<Rate> {rate2}, UserId = 2};
            var account = new List<Account>
            {
                acc,
                acc1
            };

            account.ForEach(d => context.Accounts.Add(d));
            context.SaveChanges();

            var news1 = new News
            {
                Id = 1,
                Author = "Admin",
                PublicationDate = DateTime.Now.AddMonths(-1),
                Text =
                    "We are very pleased with the performance of our newly acquired assets and our achievement of annualized cost " +
                    "synergies of $1 billion in the second quarter.We now expect annual cost synergies related to the acquisition of " +
                    "$1.25 billion,up from our original estimate of $700 million,said Dan McCarthy, ApexHome President and Chief Executive Officer." +
                    "As we move forward, we are continuing to focus on executing our strategy for growth, including upgrading our broadband speed" +
                    " capabilities, expanding our new Vantage video service to an increasing portion of our footprint, and implementing our successful" +
                    " commercial distribution capabilities in ApexHome's new markets. We will remain focused on increasing our broadband and video penetration," +
                    " and improving our efficiency. Our priorities continue to be driving strong free cash flow and continuing our disciplined capital allocation policy," +
                    " which together underpin our very attractive, sustainable dividend, and industry-leading dividend payout ratio. We also are very well-positioned to achieve" +
                    " our plan to reduce leverage over time",
                Title = "Planned work"
            };
            var news2 = new News
            {
                Id = 2,
                Author = "Admin",
                PublicationDate = DateTime.Now.AddDays(-18),
                Text =
                    "Sorry, yesterday our site was unavailable.Technical problems can either ruin a theatrical performance or make " +
                    "the audience even more responsive to the performers on stage. Usually, the audience is with you. " +
                    "The type of technical problems a production can face today has changed drastically from years ago as computers " +
                    "have increased their role in the theater. Projections,lighting and scenic pieces don't shift or move without some" +
                    " computer assistance, at least on Broadway and in touring Broadway shows.",
                Title = "Interruptions in the work of site"
            };
            var news3 = new News
            {
                Id = 3,
                Author = "Moderator",
                PublicationDate = DateTime.Now.AddDays(-25),
                Text =
                    "HOLIDAYS. Holidays are 'holy days,' when people interrupt the profane, mundane round of production and celebrate with " +
                    "the preparation and eating of special foods and meals. The two basic forms of holidays are a festival (from Latin festum " +
                    "for 'feast'), when people break their normal weekly, monthly, or annual routine to celebrate together, and a vacation (in the " +
                    "sense of leaving their homes and workplaces empty), when an often longer disruption may be accompanied by dislocation, as people " +
                    "change residences or travel.",
                Title = "Holidays and Shares"
            };

            var news4 = new News
            {
                Id = 4,
                Author = "Admin",
                PublicationDate = DateTime.Now.AddMonths(-2),
                Text =
                    "A computer is a device that can be instructed to carry out an arbitrary set of arithmetic or logical operations automatically. Their ability of computers to follow a sequence of operations, called a program, make computers very flexible and useful. Such computers are used as control systems for a very wide variety of industrial and consumer devices. This includes simple special purpose devices like microwave ovens and remote controls, factory devices such as industrial robots and computer assisted design, but also in general purpose devices like personal computers and mobile devices such as smartphones. The Internet is run on computers and it connects millions of other computers.",
                Title = "Computer"
            };

            var news = new List<News>
            {
                news1,
                news2,
                news3,
                news4
            };

            news.ForEach(d => context.Newses.Add(d));
            context.SaveChanges();

            var comm1 = new Comment
            {
                Id = 1,
                AuthorName = "Alex",
                Time = DateTime.Now.AddDays(-5),
                UserComm = "Great News!!!",
                NewsId = 1
            };
            var comm2 = new Comment
            {
                Id = 2,
                AuthorName = "rock@mail.ru",
                Time = DateTime.Now.AddDays(-15),
                UserComm = "Yes",
                NewsId = 1
            };
            var comm3 = new Comment
            {
                Id = 3,
                AuthorName = "power@gmail.com",
                Time = DateTime.Now.AddDays(-2),
                UserComm = "Bad News!!!",
                NewsId = 1
            };

            var comment = new List<Comment>
            {
                comm1,
                comm2,
                comm3
            };
            comment.ForEach(d => context.Comments.Add(d));
            context.SaveChanges();

            var map = new List<Map>
            {
                new Map {Id = 1, GeoLat = 49.9935, GeoLong = 36.230383},
                new Map {Id = 2, GeoLat = 49.588267, GeoLong = 34.551417},
                new Map {Id = 3, GeoLat = 50.9077, GeoLong = 34.7981},
                new Map {Id = 4, GeoLat = 49.9627279, GeoLong = 33.6053331},
                new Map {Id = 5, GeoLat = 50.3150962, GeoLong = 34.9005644},
                new Map {Id = 6, GeoLat = 49.2121445, GeoLong = 37.2665006},
                new Map {Id = 7, GeoLat = 48.8532, GeoLong = 37.6053},
                new Map {Id = 8, GeoLat = 49.6944064, GeoLong = 36.3595476}
            };
            map.ForEach(d => context.Maps.Add(d));
            context.SaveChanges();

            var advant1 = new Advantage
            {
                Id = 1,
                Name = "High speed internet",
                Text =
                    "The Internet is the global system of interconnected computer networks that use the Internet protocol suite (TCP/IP) to link billions of devices worldwide. It is a network of networks that consists of millions of private, public, academic, business, and government networks of local to global scope, linked by a broad array of electronic, wireless, and optical networking technologies. The Internet carries an extensive range of information resources and services, such as the inter-linked hypertext documents and applications of the World Wide Web (WWW), electronic mail, voice over IP telephony, and peer-to-peer networks for file sharing."
            };

            var adv2 = new Advantage
            {
                Id = 2,
                Name = "24 hour support",
                Text =
                    "In commerce and industry, 24/7 or 24-7 service (usually spoken 'twenty-four seven') is service that is available any time and, usually, every day. Alternate orthography for the numerical part includes 24x7 (usually spoken 'twenty-four by seven'). The numerals stand for '24 hours a day, 7 days a week'. Less commonly used, 24/7/52 (adding '52 weeks') and 24/7/365 service (adding '365 days') make clear that service is available every day of the year."
            };

            var adv3 = new Advantage
            {
                Id = 3,
                Name = "Free and Fast Connecting",
                Text =
                    "Connecting Point Marketing Group (CPMG) organizes and hosts trade Events for executives in the retail, restaurant, healthcare and hotel industries for innovation.Each Event is conducted in a business-intensive format that optimizes strategic interaction. We invite you to forge new relationships and improve your business today by attending one of our dynamic Events below."
            };

            var adv4 = new Advantage
            {
                Id = 4,
                Name = "Security",
                Text =
                    "1 for more than 40 years: security essen is the world's most important trade fair for security and fire prevention. The world market for security is booming – it is presenting itself with all its facets in Essen.From fire prevention and cyber security and CCTV to the protection of outdoor areas: experts, decision - makers and buyers from this industry will be meeting in Essen.At top international level, with the latest trends and the most important innovations."
            };

            var adv5 = new Advantage
            {
                Id = 5,
                Name = "Cheap packages",
                Text =
                    "Connect your home to what matters most with a TV + Internet bundle and get the most out of your satellite TV service! Enjoy your favorite entertainment channels with satellite television service from DIRECTV and keep all of your devices connected with high-speed Internet service from some of the nation’s leading providers like AT&T. A ApexTV + Internet bundle also allows you to get the most out of your TV service with ApexHome On Demand and Interactive features.Call today to learn more about the available TV and Internet bundle offers in your area and to find the right package for you!"
            };
            var advantage = new List<Advantage>
            {
                advant1,
                adv2,
                adv3,
                adv4,
                adv5
            };
            advantage.ForEach(d => context.Advantages.Add(d));
            context.SaveChanges();

            var photo = new List<Picture>
            {
                new Picture
                {
                    Id = 1,
                    PicturePath = "~/Images/pict1.jpg",
                    NewsId = 1
                },
                new Picture
                {
                    Id = 2,
                    PicturePath = "~/Images/pict2.png",
                    NewsId = 2
                },
                new Picture
                {
                    Id = 3,
                    PicturePath = "~/Images/pict3.jpg",
                    NewsId = 3
                },
                new Picture
                {
                    Id = 4,
                    PicturePath = "~/Images/computer.jpg",
                    NewsId = 4
                },
                new Picture
                {
                    Id = 4,
                    PicturePath = "~/Images/picture1.png",
                    AdvantageId = 1
                },
                new Picture
                {
                    Id = 5,
                    PicturePath = "~/Images/picture2.png",
                    AdvantageId = 2
                },
                new Picture
                {
                    Id = 6,
                    PicturePath = "~/Images/picture3.png",
                    AdvantageId = 3
                },
                new Picture
                {
                    Id = 7,
                    PicturePath = "~/Images/package.png",
                    AdvantageId = 4
                },
                new Picture
                {
                    Id = 8,
                    PicturePath = "~/Images/pack2.png",
                    AdvantageId = 5
                }
            };
            photo.ForEach(d => context.Pictures.Add(d));
            context.SaveChanges();
        }

        public class SimpleMembershipInitializer
        {
            public SimpleMembershipInitializer()
            {
                if (!WebSecurity.Initialized)
                    WebSecurity.InitializeDatabaseConnection("InternetProvider", "User", "Id", "Email", true);
            }
        }
    }
}