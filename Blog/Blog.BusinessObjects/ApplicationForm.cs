﻿namespace BusinessObjects
{
    public class ApplicationForm
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Pay { get; set; }
        public bool DeliveryHome { get; set; }
        public bool DeliveryOffice { get; set; }
    }
}