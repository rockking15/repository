﻿namespace BusinessObjects
{
    public class Vote
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int Sum { get; set; }
    }
}