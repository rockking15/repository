﻿using System;
using System.Collections.Generic;

namespace BusinessObjects
{
    public class Post
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public DateTime PostDate { get; set; }
        public string Creator { get; set; }
        public int NumberComments { get; set; }
        public int PostGroupId { get; set; }
        public byte[] Photo { get; set; }
        public virtual PostGroup PostGroup { get; set; }
        public virtual ICollection<Tag> Tags { get; set; }
    }
}