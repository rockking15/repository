﻿using System.Collections.Generic;

namespace BusinessObjects
{
    public class PostGroup
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public virtual ICollection<Post> Posts { get; set; }
    }
}