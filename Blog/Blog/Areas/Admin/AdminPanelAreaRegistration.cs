﻿using System.Web.Mvc;

namespace Blog.Areas.Admin
{
    public class AdminPanelAreaRegistration : AreaRegistration
    {
        public override string AreaName => "Admin";

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Admin",
                "Admin/{controller}/{action}",
                new {area = "Admin", controller = "Admin", action = "Index", id = UrlParameter.Optional},
                new[] {"Blog.Areas.Admin.Controllers"}
                );
        }
    }
}