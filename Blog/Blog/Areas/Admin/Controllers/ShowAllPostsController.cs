﻿using System.Web.Mvc;
using Blog.IRepository;
using Blog.ViewModel;

namespace Blog.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin,Moderator")]
    public class ShowAllPostsController : Controller
    {
        private readonly IRepo _repo;

        public ShowAllPostsController()
        {
            _repo = new Repository.Repository();
        }

        [HttpGet]
        public ActionResult ShowAll()
        {
            var model = new PostsViewModel {Posts = _repo.GetPosts()};
            return View(model);
        }
    }
}