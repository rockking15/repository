﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using Blog.IRepository;
using Blog.ViewModel;
using BusinessObjects;

namespace Blog.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin,Moderator")]
    public class AdminController : Controller
    {
        private readonly IRepo _repo;

        public AdminController()
        {
            _repo = new Repository.Repository();
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Create()
        {
            var model = new PostsViewModel {Tags = _repo.GetTags()};
            var list = new SelectList(_repo.GetGroups(), "Id", "Title");
            ViewBag.List = list;
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PostsViewModel model, HttpPostedFileBase imageFile, int postGroup,
            List<int> selectedTags)
        {
            try
            {
                var buf = new byte[imageFile.ContentLength];
                imageFile.InputStream.Read(buf, 0, buf.Length);

                var post = new Post
                {
                    Creator = model.Creator,
                    Photo = buf,
                    PostDate = DateTime.Now,
                    Text = model.Text,
                    Title = model.Title,
                    PostGroupId = postGroup
                };

                _repo.CreatePost(post, selectedTags);
                try
                {
                    _repo.Save();
                }
                catch (Exception ex)
                {
                    var b = ex.ToString();
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View(model);
            }
        }

        [HttpGet]
        public ActionResult Delete()
        {
            return View();
        }


        [HttpPost]
        public ActionResult Delete(int artnumber)
        {
            if (ModelState.IsValid)
            {
                _repo.DeletePost(artnumber);
                _repo.Save();
                return RedirectToAction("Index");
            }

            return View();
        }
    }
}