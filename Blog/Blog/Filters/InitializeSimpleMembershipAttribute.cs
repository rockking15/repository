﻿using System;
using System.Web.Mvc;
using System.Web.Security;
using WebMatrix.WebData;

namespace Blog.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public sealed class InitializeSimpleMembershipAttribute : ActionFilterAttribute
    {
        public void SimpleMembershipInitializer()
        {
            WebSecurity.InitializeDatabaseConnection("Blog", "User", "Id", "Email", true);

            var roles = (SimpleRoleProvider) Roles.Provider;
            var membership = (SimpleMembershipProvider) Membership.Provider;

            if (!roles.RoleExists("Admin"))
            {
                roles.CreateRole("Admin");
            }

            if (!roles.RoleExists("Moderator"))
            {
                roles.CreateRole("Moderator");
            }

            if (membership.GetUser("admin", false) == null)

            {
                WebSecurity.CreateUserAndAccount("admin@mail.ru", "123123",
                    new {UserName = "admin", DateOfBirthday = 01/01/1990});

                roles.AddUsersToRoles(new[] {"admin@mail.ru"}, new[] {"Admin"});
            }
        }
    }
}