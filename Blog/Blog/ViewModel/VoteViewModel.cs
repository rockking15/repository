﻿using System.Collections.Generic;
using BusinessObjects;

namespace Blog.ViewModel
{
    public class VoteViewModel
    {
        public IEnumerable<Vote> Votes { get; set; }
    }
}