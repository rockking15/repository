﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using BusinessObjects;

namespace Blog.ViewModel
{
    public class PostsViewModel
    {
        public IEnumerable<Post> Posts { get; set; }
        public IEnumerable<Tag> Tags { get; set; }
        public List<Vote> Votes { get; set; }
        public List<string> SelectedTags { get; set; }
        public int SelectedResponse { get; set; }

        [Required]
        public string Title { get; set; }

        [StringLength(1150, MinimumLength = 201)]
        [Required]
        public string Text { get; set; }

        public DateTime PostDate { get; set; }

        [Required]
        public string Creator { get; set; }

        public int NumberComments { get; set; }
        public int PostGroupId { get; set; }
        public byte[] Photo { get; set; }
    }
}