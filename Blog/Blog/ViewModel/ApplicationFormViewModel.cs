﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Blog.ViewModel
{
    public class ApplicationFormViewModel
    {
        [Required(ErrorMessage = "Name is empty or more than 50 characters")]
        [StringLength(50, MinimumLength = 3)]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage = "Incorrect email address")
        ]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(25, MinimumLength = 8, ErrorMessage = "field must be atleast 8 characters")]
        [Display(Name = "Phone")]
        public string Phone { get; set; }

        [Required]
        public string Pay { get; set; }

        public bool DeliveryHome { get; set; }
        public bool DeliveryOffice { get; set; }

        public List<string> ListForCustomHelper { get; set; }
    }
}