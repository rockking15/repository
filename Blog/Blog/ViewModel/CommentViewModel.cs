﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using BusinessObjects;

namespace Blog.ViewModel
{
    public class CommentViewModel
    {
        [Required(ErrorMessage = "Name is empty or more than 50 characters")]
        [StringLength(50, MinimumLength = 3)]
        [Display(Name = "AuthorName")]
        public string AuthorName { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime Time { get; set; }

        [Required]
        public string UserComm { get; set; }

        public List<Comment> Comments { get; set; }
    }
}