﻿using System.Collections.Generic;
using System.Text;
using System.Web.Mvc;

namespace Blog.Helpers
{
    public static class Listhelper
    {
        public static MvcHtmlString ListHelper(this HtmlHelper helper, List<string> items)
        {
            var tag = new StringBuilder();

            tag.Append("<ul>");

            foreach (var item in items)
            {
                tag.Append($"<li>{item}</li>");
            }
            tag.Append("</ul>");

            return new MvcHtmlString(tag.ToString());
        }
    }
}