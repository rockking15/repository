﻿using System.Web.Mvc;
using System.Web.Security;
using Blog.Filters;
using Blog.Models;
using WebMatrix.WebData;

namespace Blog.Controllers
{
    [InitializeSimpleMembership]
    public class RegistrationController : Controller
    {
        // GET: Registration
        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }


        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    WebSecurity.CreateUserAndAccount(model.Email, model.Password,
                        new {Username = model.UserName, DateOfBirthday = model.Year});
                    WebSecurity.Login(model.Email, model.Password);

                    Roles.AddUsersToRole(new[] {model.Email}, "User");
                    return RedirectToAction("Index", "Home");
                }
                catch (MembershipCreateUserException e)
                {
                    ModelState.AddModelError("", ErrorCodeToString(e.StatusCode));
                    ViewBag.Error = ErrorCodeToString(e.StatusCode);
                }
            }

            return View(model);
        }


        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "Email уже существует. Введите другой Email.";

                default:
                    return
                        "Произошла неизвестная ошибка. Проверьте введенное значение и повторите попытку. Если проблему устранить не удастся, обратитесь к системному администратору.";
            }
        }
    }
}