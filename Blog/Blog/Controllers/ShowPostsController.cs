﻿using System.Web.Mvc;
using Blog.ViewModel;

namespace Blog.Controllers
{
    public class ShowPostsController : Controller
    {
        private readonly Repository.Repository _db = new Repository.Repository();
        private readonly PostsViewModel _model = new PostsViewModel();

        public ActionResult UserPosts(string name)
        {
            _model.Posts = _db.GetUserPosts(name);
            return View("ShowAll", _model);
        }

        public ActionResult GroupPosts(string title)
        {
            _model.Posts = _db.GetPostsFromGroup(title);
            return View("ShowAll", _model);
        }

        public ActionResult PostsTags(int tagId)
        {
            _model.Posts = _db.GetPostsTags(tagId);
            return View("ShowAll", _model);
        }

        public ViewResult ShowAll(PostsViewModel model)
        {
            return View(model);
        }
    }
}