﻿using System.Linq;
using System.Web.Mvc;
using Blog.IRepository;
using Blog.ViewModel;

namespace Blog.Controllers
{
    public class HomeController : Controller
    {
        private readonly IRepo _repo;

        public HomeController()
        {
            _repo = new Repository.Repository();
        }

        [HttpGet]
        public ViewResult Index()
        {
            var model = new PostsViewModel();
            model.Posts = _repo.GetPosts();
            model.Votes = _repo.GetVotes();
            return View(model);
        }

        [HttpPost]
        public ActionResult Vote(PostsViewModel model)
        {
            if (ModelState.IsValid)
            {
                _repo.SaveVote(model.SelectedResponse, _repo.Increase(model.SelectedResponse));
                _repo.Save();
                var partmodel = new VoteViewModel();
                partmodel.Votes = _repo.GetVotes();
                return PartialView("_VotePartial", partmodel);
            }

            return View("Index", model);
        }

        [HttpGet]
        public ActionResult MoreDetails(int? id)
        {
            var p = _repo.GetPosts().First(x => x.Id == id);
            return PartialView("_FullText", p);
        }
    }
}