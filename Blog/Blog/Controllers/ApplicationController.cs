﻿using System.Collections.Generic;
using System.Web.Mvc;
using Blog.ViewModel;
using BusinessObjects;

namespace Blog.Controllers
{
    public class ApplicationController : Controller
    {
        private readonly Repository.Repository _db = new Repository.Repository();

        public ActionResult Index()
        {
            var model = new ApplicationFormViewModel();
            model.ListForCustomHelper = new List<string> {"Candy", "Cookies", "Coffee"};
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Result(ApplicationFormViewModel model)
        {
            TempData["model"] = model;
            if (ModelState.IsValid)
            {
                var item = new ApplicationForm
                {
                    Name = model.Name,
                    Email = model.Email,
                    Phone = model.Phone,
                    Pay = model.Pay,
                    DeliveryHome = model.DeliveryHome,
                    DeliveryOffice = model.DeliveryOffice
                };
                _db.SaveDataApplication(item);
                _db.Save();
                return RedirectToAction("Index", "Result");
            }
            return RedirectToAction("Index");
        }
    }
}