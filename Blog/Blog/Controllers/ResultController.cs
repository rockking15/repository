﻿using System.Web.Mvc;
using Blog.ViewModel;

namespace Blog.Controllers
{
    public class ResultController : Controller
    {
        public ActionResult Index()
        {
            var model = (ApplicationFormViewModel) TempData["model"];
            return View(model);
        }
    }
}