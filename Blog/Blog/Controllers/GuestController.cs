﻿using System;
using System.Web.Mvc;
using Blog.ViewModel;
using BusinessObjects;

namespace Blog.Controllers
{
    public class GuestController : Controller
    {
        private readonly Repository.Repository _db = new Repository.Repository();

        [HttpGet]
        public ActionResult Index()
        {
            var model = new CommentViewModel {Time = DateTime.Now};
            model.Comments = _db.GetComments();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(CommentViewModel model)
        {
            if (ModelState.IsValid)
            {
                var item = new Comment
                {
                    AuthorName = model.AuthorName,
                    Time = model.Time,
                    UserComm = model.UserComm
                };
                _db.SaveComment(item);
                _db.Save();
                return RedirectToAction("Index");
            }
            return View(model);
        }
    }
}