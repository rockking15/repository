﻿using System.ComponentModel.DataAnnotations;

namespace Blog.Models
{
    public class LogInModel
    {
        [Required]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public bool RememberMe { get; set; }
    }
}