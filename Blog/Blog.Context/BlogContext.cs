﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using BusinessObjects;

namespace Context
{
    public class BlogContext : DbContext
    {
        public BlogContext()
            : base("name=Blog")
        {
        }

        public virtual DbSet<ApplicationForm> ApplicationForms { get; set; }
        public virtual DbSet<Comment> Comments { get; set; }
        public virtual DbSet<Post> Posts { get; set; }
        public virtual DbSet<PostGroup> PostGroups { get; set; }
        public virtual DbSet<Vote> Votes { get; set; }
        public virtual DbSet<Tag> Tags { get; set; }
        public virtual DbSet<User> Users { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}