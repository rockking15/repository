﻿using System;
using System.Collections.Generic;
using BusinessObjects;

namespace Blog.IRepository
{
    public interface IRepo : IDisposable
    {
        List<Post> GetPosts();
        List<Vote> GetVotes();
        void SaveComment(Comment model);
        void SaveDataApplication(ApplicationForm model);
        int Increase(int id);
        void SaveVote(int id, int sum);
        void Save();
        List<Post> GetUserPosts(string name);
        List<Post> GetPostsFromGroup(string title);
        List<Post> GetPostsTags(int tagId);
        List<Comment> GetComments();
        void CreatePost(Post model, List<int> selectedTags);
        void DeletePost(int id);
        List<PostGroup> GetGroups();
        List<Tag> GetTags();
    }
}