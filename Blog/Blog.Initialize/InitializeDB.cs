﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Threading;
using System.Web;
using System.Web.Security;
using BusinessObjects;
using Context;
using WebMatrix.WebData;

namespace Initialize
{
    public class InitializeDb : DropCreateDatabaseIfModelChanges<BlogContext>
    {
        private static SimpleMembershipInitializer _initializer;
        private static object _initializerLock = new object();
        private static bool _isInitialized;

        protected override void Seed(BlogContext context)
        {
            LazyInitializer.EnsureInitialized(ref _initializer, ref _isInitialized, ref _initializerLock);
            var roles = (SimpleRoleProvider) Roles.Provider;
            var membership = (SimpleMembershipProvider) Membership.Provider;

            if (!roles.RoleExists("Admin"))
            {
                roles.CreateRole("Admin");
            }

            if (!roles.RoleExists("User"))
            {
                roles.CreateRole("User");
            }

            if (!roles.RoleExists("Moderator"))
            {
                roles.CreateRole("Moderator");
            }

            if (membership.GetUser("admin", false) == null)

            {
                WebSecurity.CreateUserAndAccount("admin@mail.ru", "123123",
                    new {UserName = "admin", DateOfBirthday = 01/01/1990});

                roles.AddUsersToRoles(new[] {"admin@mail.ru"}, new[] {"Admin"});
            }

            var user = new List<ApplicationForm>
            {
                new ApplicationForm
                {
                    Id = 1,
                    Name = "FirstUser",
                    Email = "rockking151@gmail.com",
                    Phone = "123456789",
                    Pay = "Money",
                    DeliveryOffice = false,
                    DeliveryHome = true
                },
                new ApplicationForm
                {
                    Id = 2,
                    Name = "SecondUser",
                    Email = "yandex@mail.ru",
                    Phone = "00000001",
                    Pay = "Card",
                    DeliveryOffice = true,
                    DeliveryHome = false
                }
            };

            user.ForEach(d => context.ApplicationForms.Add(d));
            context.SaveChanges();

            var comment = new List<Comment>
            {
                new Comment
                {
                    Id = 1,
                    AuthorName = "Admin",
                    Time = DateTime.Now,
                    UserComm = "BlaBlaBla"
                },
                new Comment
                {
                    Id = 2,
                    AuthorName = "Admin",
                    Time = DateTime.Now,
                    UserComm = "Yes"
                }
            };

            comment.ForEach(d => context.Comments.Add(d));
            context.SaveChanges();

            var postGroup = new List<PostGroup>
            {
                new PostGroup {Id = 1, Title = "Tutorials"},
                new PostGroup {Id = 2, Title = "Photos"},
                new PostGroup {Id = 3, Title = "Interesting"}
            };
            postGroup.ForEach(d => context.PostGroups.Add(d));
            context.SaveChanges();

            var tag1 = new Tag {Id = 1, Name = "Tutorial"};
            var tag2 = new Tag {Id = 2, Name = "C#"};
            var tag3 = new Tag {Id = 3, Name = "Start"};
            var tag4 = new Tag {Id = 4, Name = "Relax"};
            var tag5 = new Tag {Id = 5, Name = "Whisky"};

            var tag = new List<Tag>
            {
                tag1,
                tag2,
                tag3,
                tag4,
                tag5
            };

            tag.ForEach(d => context.Tags.Add(d));
            context.SaveChanges();

            var path1 = HttpContext.Current.Server.MapPath("~/Images/photo1.png");
            var path2 = HttpContext.Current.Server.MapPath("~/Images/photo2.png");
            var path3 = HttpContext.Current.Server.MapPath("~/Images/photo3.png");

            var post = new List<Post>
            {
                new Post
                {
                    Id = 1,
                    Title = "C# Tutorial №1",
                    Creator = "Admin",
                    PostDate = DateTime.Now.AddDays(-1),
                    PostGroupId = 1,
                    Text =
                        "The C# tutorials provide an overview of the basics of the language and identify important language feature." +
                        "Each tutorial includes one or more sample programs. The tutorials discuss the sample code plus provide additional background information." +
                        "Welcome to this C# Tutorial, currently consisting of 49 articles covering all the most important C# concepts. This tutorial is primarily for new users of this great technology," +
                        " and we recommend you to go through all the chapters, to get the most out of it as possible. While each chapter can be used without reading the previous chapters, some of them may" +
                        " reference things done in earlier chapters.Have a look at the Table of contents to the right, where all the chapters are listed. This tutorial is never done - we will keep adding new " +
                        "stuff to it, so check back regularly. We hope this tutorial helps you to get started with C#.Everything here is free, and we hope you like our work. Enjoy! ",
                    Photo = File.ReadAllBytes(path1),
                    NumberComments = 12,
                    Tags = new List<Tag>
                    {
                        tag1,
                        tag2,
                        tag5
                    }
                },
                new Post
                {
                    Id = 2,
                    Title = "America",
                    Creator = "Photographer",
                    PostDate = DateTime.Now.Date.AddDays(-1).AddHours(17),
                    PostGroupId = 2,
                    Text =
                        "The country has three major mountain ranges. The Appalachians extend from Canada to the state of Alabama," +
                        " a few hundred miles west of the Atlantic Ocean. They are the oldest of the three mountain ranges and offer " +
                        "spectacular sightseeing and excellent camping spots. The Rockies are, on average, the highest in North America, " +
                        "extending from Alaska to New Mexico, with many areas protected as national parks. They offer hiking, camping, " +
                        "skiing, and sightseeing opportunities. The combined Sierra Nevada and Cascade ranges are the youngest. " +
                        "The Sierras extend across the 'backbone' of California, with sites such as Lake Tahoe and Yosemite National Park, " +
                        "then give way to the even younger volcanic Cascade range, with some of the highest points in the country.",
                    Photo = File.ReadAllBytes(path2),
                    NumberComments = 25,
                    Tags = new List<Tag>
                    {
                        tag2,
                        tag3,
                        tag5
                    }
                },
                new Post
                {
                    Id = 3,
                    Title = "Relax",
                    Creator = "Admin",
                    PostDate = DateTime.Now,
                    PostGroupId = 3,
                    Text =
                        "How is single malt whisky made? How do you taste whisky? Do I add water or ice? How do I pronounce the funny Scottish names? " +
                        "There are so many questions to answer but by learning the simple basics, it can quickly become much easier to navigate your way" +
                        " through the rich and diverse world of whisky. These basics are the foundation on which your whisky knowledge will be built. There" +
                        " is so much history and tradition associated with the world of single malt whisky, blended whiskys and American bourbons. How do you " +
                        "find out about it all? Who can help? Where can you buy those rare bottles? There are many different ways to find out information – through" +
                        " a website or blog (like ours!), books and magazines, local or national whisky societies or associations and specialist whisky shops. ",
                    Photo = File.ReadAllBytes(path3),
                    NumberComments = 22,
                    Tags = new List<Tag>
                    {
                        tag1,
                        tag4,
                        tag3
                    }
                }
            };
            post.ForEach(d => context.Posts.Add(d));
            context.SaveChanges();


            var vote = new List<Vote>
            {
                new Vote {Id = 1, Title = "Java", Sum = 200},
                new Vote {Id = 2, Title = "C++", Sum = 150},
                new Vote {Id = 3, Title = "C#", Sum = 450},
                new Vote {Id = 4, Title = "Python", Sum = 50}
            };

            vote.ForEach(d => context.Votes.Add(d));
            context.SaveChanges();
        }
    }

    public class SimpleMembershipInitializer
    {
        public SimpleMembershipInitializer()
        {
            if (!WebSecurity.Initialized)
                WebSecurity.InitializeDatabaseConnection("Blog", "User", "Id", "Email", true);
        }
    }
}