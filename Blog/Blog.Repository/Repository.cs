﻿using System;
using System.Collections.Generic;
using System.Linq;
using Blog.IRepository;
using BusinessObjects;
using Context;

namespace Repository
{
    public class Repository : IRepo
    {
        private readonly BlogContext _context;
        private bool _disposed;

        public Repository()
        {
            _context = new BlogContext();
        }

        public List<Post> GetPosts()
        {
            return _context.Posts.ToList();
        }

        public List<Vote> GetVotes()
        {
            return _context.Votes.ToList();
        }

        public void SaveComment(Comment model)
        {
            var item = new Comment
            {
                AuthorName = model.AuthorName,
                Time = model.Time,
                UserComm = model.UserComm
            };
            _context.Comments.Add(item);
        }

        public void SaveDataApplication(ApplicationForm model)
        {
            var item = new ApplicationForm
            {
                Name = model.Name,
                Email = model.Email,
                Pay = model.Pay,
                Phone = model.Phone,
                DeliveryOffice = model.DeliveryOffice,
                DeliveryHome = model.DeliveryHome
            };
            _context.ApplicationForms.Add(item);
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public List<Post> GetUserPosts(string name)
        {
            return _context.Posts.Where(x => x.Creator == name).ToList();
        }

        public List<Post> GetPostsFromGroup(string title)
        {
            return _context.Posts.Where(x => x.PostGroup.Title == title).ToList();
        }

        public List<Post> GetPostsTags(int tagId)
        {
            var tags = _context.Tags.Find(tagId);

            var posts = tags.Posts;

            var list = new List<Post>();
            list.AddRange(posts);

            return list;
        }

        public List<Comment> GetComments()
        {
            return _context.Comments.ToList();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public int Increase(int id)
        {
            var p = (from t in _context.Votes
                where t.Id == id
                select t.Sum).FirstOrDefault();

            p = p + 1;
            return p;
        }

        public void SaveVote(int id, int sum)
        {
            _context.Votes.Where(t => t.Id == id).ToList().ForEach(x => x.Sum = sum);
        }

        public void CreatePost(Post post, List<int> selectedTags)
        {
            var list = new List<Tag>();
            for (var i = 0; i < selectedTags.Count; i++)
            {
                var b = _context.Tags.Find(selectedTags[i]);
                list.Add(b);
            }

            post.Tags = list;
            _context.Posts.Add(post);
        }

        public void DeletePost(int id)
        {
            _context.Posts.Remove(_context.Posts.Find(id));
        }

        public List<PostGroup> GetGroups()
        {
            return _context.PostGroups.ToList();
        }

        public List<Tag> GetTags()
        {
            return _context.Tags.ToList();
        }

        public virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }
    }
}